//
//  CNTCacheWarehouse.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

// MARK: Declaration
/// Manager for caching objects to disk.
public final class CNTCacheWarehouse: Codable {
    /// Folder url for cached files.
    public let cacheUrl: URL
    
    /// Unique keys for cached files.
    public private(set) var cachedKeys: Set<String>
    
    internal let defaultFolderName: String
    internal let headerFolderKey: String
    internal let objectFolderKey: String
    
    public convenience init(from decoder: Decoder) throws {
        let cacheUrl = try CNTDataWarehouse.shared.applicationUrl.appending(created: "cache")
        let container = try decoder.container(keyedBy: DecodeKeys.self)
        let cachedKeys = try container.decode(Set<String>.self, forKey: .cachedKeys)
        
        self.init(cacheUrl: cacheUrl, cachedKeys: cachedKeys)
    }
    
    private init(cacheUrl: URL, cachedKeys: Set<String> = []) {
        self.cacheUrl = cacheUrl
        self.cachedKeys = cachedKeys
        
        defaultFolderName = "default"
        headerFolderKey = "header"
        objectFolderKey = "object"
    }
    
    private enum DecodeKeys: String, CodingKey {
        case cachedKeys
    }
}

// MARK: Shared
extension CNTCacheWarehouse {
    /// CacheManager singletone object.
    public static let shared = { () -> CNTCacheWarehouse in
        do {
            return try CNTDataWarehouse.shared.load(from: url)
        } catch {
            do {
                let cacheUrl = try CNTDataWarehouse.shared.applicationUrl.appending(created: "cache")
                
                return CNTCacheWarehouse(cacheUrl: cacheUrl)
            } catch {
                fatalError(error.info("\(CNTCacheWarehouse.self): Couldn't create cache folder. \(CNTCacheWarehouse.self) not initialized."))
            }
        }
    }()
    
    private static let url = CNTDataWarehouse.shared.applicationUrl.appending("\(CNTCacheWarehouse.self)")
}

extension CNTCacheWarehouse {
    internal func enqueue(key: String) throws {
        let isNoNeedToSave = cachedKeys.contains(key)

        cachedKeys.insert(key)

        if isNoNeedToSave {
            return
        }
        
        try save()
    }

    internal func remove(key: String) throws {
        if cachedKeys.remove(key) == nil {
            return
        }
        
        try save()
    }

    internal func save() throws {
        try CNTDataWarehouse.shared.save(self, to: CNTCacheWarehouse.url)
    }
}

