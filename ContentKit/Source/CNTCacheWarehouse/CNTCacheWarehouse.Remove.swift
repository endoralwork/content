//
//  CNTCacheWarehouse.Remove.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

extension CNTCacheWarehouse {
    /// Remove cached file from disk.
    /// - Parameters:
    ///    - key: Key of the cached object to be removed.
    ///    - languageCode: Language code in the lowercase for removing cached file by specified key. For example: "en".
    public func remove(objectFor key: String, languageCode: String? = nil) throws {
        try remove(key: key)
        try CNTDataWarehouse.shared.remove(from: headerUrl(for: key, languageCode: languageCode))
        try CNTDataWarehouse.shared.remove(from: objectUrl(for: key, languageCode: languageCode))
    }
}

