//
//  CNTCacheWarehouse.Enqueue.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import UIKit

// MARK: Codable
extension CNTCacheWarehouse {
    /// Write object to disk as codable object.
    /// - Parameters:
    ///    - object: Object caching.
    ///    - key: Unique string value to write object to disk.
    ///    - version: Integer value of the version of the object. Used when reading an object from disk for comparison with the requested version of the object.
    ///    - languageCode: String-valued language code in lowercase. It is part of the final path when writing an object to disk. Default value is `nil`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - encoder: Encoder used to encode data into an object. Default value is `JSONEncoder()`.
    @discardableResult
    public func enqueue<T: Codable>(_ object: T?, key: String, version: Int, languageCode: String? = nil, writingOptions: Data.WritingOptions = [], encoder: AnyEncoder = JSONEncoder()) throws -> EnqueueResult {
        guard let object else {
            throw CNTError(
                description: "Invalid object.",
                reason: "Failed to enqueue “nil“ object. Key=“\(key)“, version=“\(version)“, languageCode=“\(languageCode ?? "nil")“, writingOptions=“\(writingOptions)“, encoder=“\(encoder)“."
            )
        }
        
        return try enqueue(object, key: key, version: version, languageCode: languageCode, writingOptions: writingOptions, encoder: encoder)
    }
    
    /// Write object to disk as codable object.
    /// - Parameters:
    ///    - object: Object caching.
    ///    - key: Unique string value to write object to disk.
    ///    - version: Integer value of the version of the object. Used when reading an object from disk for comparison with the requested version of the object.
    ///    - languageCode: String-valued language code in lowercase. It is part of the final path when writing an object to disk. Default value is `nil`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - encoder: Encoder used to encode data into an object. Default value is `JSONEncoder()`.
    @discardableResult
    public func enqueue<T: Codable>(_ object: T, key: String, version: Int, languageCode: String? = nil, writingOptions: Data.WritingOptions = [], encoder: AnyEncoder = JSONEncoder()) throws -> EnqueueResult {
        let headerUrl = try createHeaderUrl(for: key, languageCode: languageCode)
        let objectUrl = try createObjectUrl(for: key, languageCode: languageCode)
        
        try enqueue(key: key)
        
        try CNTDataWarehouse.shared.save(Header(for: key, version: version), to: headerUrl)
        try CNTDataWarehouse.shared.save(object, to: objectUrl, writingOptions: writingOptions, encoder: encoder)
        
        return (headerUrl, objectUrl)
    }
}

// MARK: UIImage
extension CNTCacheWarehouse {
    /// Write object to disk as image.
    /// - Parameters:
    ///    - image: Image caching.
    ///    - key: Unique string value to write object to disk.
    ///    - version: Integer value of the version of the object. Used when reading an object from disk for comparison with the requested version of the object.
    ///    - languageCode: String-valued language code in lowercase. It is part of the final path when writing an object to disk. Default value is `nil`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - dataType: Data format that contains in the specified image. Default value is `.png`.
    @discardableResult
    public func enqueue(_ image: UIImage, key: String, version: Int, languageCode: String? = nil, writingOptions: Data.WritingOptions = [], dataType: ImageDataType = .png) throws -> EnqueueResult {
        let headerUrl = try createHeaderUrl(for: key, languageCode: languageCode)
        let objectUrl = try createObjectUrl(for: key, languageCode: languageCode)
        
        try enqueue(key: key)
        
        try CNTDataWarehouse.shared.save(Header(for: key, version: version), to: headerUrl)
        try CNTDataWarehouse.shared.save(image, to: objectUrl, writingOptions: writingOptions, dataType: dataType)
        
        return (headerUrl, objectUrl)
    }
    
    public typealias ImageDataType = CNTDataWarehouse.ImageDataType
}

// MARK: NSAttributedString
extension CNTCacheWarehouse {
    /// Write object to disk as string.
    /// - Parameters:
    ///    - attributedString: Attributed string caching.
    ///    - key: Unique string value to write object to disk.
    ///    - version: Integer value of the version of the object. Used when reading an object from disk for comparison with the requested version of the object.
    ///    - languageCode: String-valued language code in lowercase. It is part of the final path when writing an object to disk. Default value is `nil`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - dict: A required dictionary specifying the document attributes. The dictionary contains values from Document Types and must at least contain documentType. Default value is `[:]`.
    @discardableResult
    public func enqueue(
        _ attributedString: NSAttributedString,
        key: String,
        version: Int,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = [],
        documentAttributes dict: Dictionary<NSAttributedString.DocumentAttributeKey, Any> = [:]
    ) throws -> EnqueueResult {
        let headerUrl = try createHeaderUrl(for: key, languageCode: languageCode)
        let objectUrl = try createObjectUrl(for: key, languageCode: languageCode)
        
        try enqueue(key: key)
        
        try CNTDataWarehouse.shared.save(Header(for: key, version: version), to: headerUrl)
        try CNTDataWarehouse.shared.save(attributedString, to: objectUrl, writingOptions: writingOptions, documentAttributes: dict)
        
        return (headerUrl, objectUrl)
    }
}

// MARK: String
extension CNTCacheWarehouse {
    /// Write object to disk as string.
    /// - Parameters:
    ///    - string: String caching.
    ///    - key: Unique string value to write object to disk.
    ///    - version: Integer value of the version of the object. Used when reading an object from disk for comparison with the requested version of the object.
    ///    - languageCode: String-valued language code in lowercase. It is part of the final path when writing an object to disk. Default value is `nil`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - encoding: The encoding used to represent the string as data. Default value is `.utf8`.
    @discardableResult
    public func enqueue(_ string: String, key: String, version: Int, languageCode: String? = nil, writingOptions: Data.WritingOptions = [], encoding: String.Encoding = .utf8) throws -> EnqueueResult {
        let headerUrl = try createHeaderUrl(for: key, languageCode: languageCode)
        let objectUrl = try createObjectUrl(for: key, languageCode: languageCode)
        
        try enqueue(key: key)
        
        try CNTDataWarehouse.shared.save(Header(for: key, version: version), to: headerUrl)
        try CNTDataWarehouse.shared.save(string, to: objectUrl, writingOptions: writingOptions, encoding: encoding)
        
        return (headerUrl, objectUrl)
    }
}

// MARK: Data
extension CNTCacheWarehouse {
    /// Write object to disk as data.
    /// - Parameters:
    ///    - data: Data caching.
    ///    - key: Unique string value to write object to disk.
    ///    - version: Integer value of the version of the object. Used when reading an object from disk for comparison with the requested version of the object.
    ///    - languageCode: String-valued language code in lowercase. It is part of the final path when writing an object to disk. Default value is `nil`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    @discardableResult
    public func enqueue(_ data: Data, key: String, version: Int, languageCode: String? = nil, writingOptions: Data.WritingOptions = []) throws -> EnqueueResult {
        let headerUrl = try createHeaderUrl(for: key, languageCode: languageCode)
        let objectUrl = try createObjectUrl(for: key, languageCode: languageCode)
        
        try enqueue(key: key)
        
        try CNTDataWarehouse.shared.save(Header(for: key, version: version), to: headerUrl)
        try CNTDataWarehouse.shared.save(data, to: objectUrl, writingOptions: writingOptions)
        
        return (headerUrl, objectUrl)
    }
}

extension CNTCacheWarehouse {
    public typealias EnqueueResult = (headerUrl: URL, objectUrl: URL)
}

