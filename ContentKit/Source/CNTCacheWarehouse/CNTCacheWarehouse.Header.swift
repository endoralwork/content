//
//  CNTCacheWarehouse.Header.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

extension CNTCacheWarehouse {
    public struct Header: Codable {
        public let version: Int
        public let key: String
        
        init(for key: String, version: Int) {
            self.version = version
            self.key = key
        }
    }
}

extension CNTCacheWarehouse {
    /// Read cache header from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the cache header associated with the object on the disk.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk.
    /// - Returns: Cached object header greater than or equal target version.
    @discardableResult
    public func header(for key: String, languageCode: String? = nil, target version: Int) throws -> Header {
        let header = try header(for: key, languageCode: languageCode)
        
        if header.version < version {
            throw CNTError(
                description: "The cached file was ignored because it has a lower version than provided.",
                reason: "Key=“\(key)“, languageCode=“\(languageCode ?? "nil")“, provided=“\(version)“, cached=“\(header.version)“."
            )
        }
        
        return header
    }
    
    /// Read cache header from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the cache header associated with the object on the disk.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk.
    /// - Returns: Cached object header.
    public func header(for key: String, languageCode: String? = nil) throws -> Header {
        let url = headerUrl(for: key, languageCode: languageCode)
        
        return try CNTDataWarehouse.shared.load(from: url)
    }
    
    /// URL to cache header of the cache the file with the specified key and with the specified language code.
    /// - Parameters:
    ///    - key: Key for caching unique for the specified language code.
    ///    - languageCode: Language code in the lowercase for caching. For example: "en".
    /// - Returns: Cached object header URL.
    public func headerUrl(for key: String, languageCode: String? = nil) -> URL {
        return cacheUrl.appending(languageCode ?? defaultFolderName).appending(headerFolderKey).appending(key)
    }
}
    
extension CNTCacheWarehouse {
    /// Create cache header on disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the cache header associated with the object on the disk.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk.
    /// - Returns: Cached object header.
    internal func createHeader(for key: String, languageCode: String?) throws -> Header {
        let url = try createHeaderUrl(for: key, languageCode: languageCode)
        
        return try CNTDataWarehouse.shared.load(from: url)
    }
    
    /// Creating a URL to cache header of the cache the file with the specified key and with the specified language code.
    /// - Parameters:
    ///    - key: Key for caching unique for the specified language code.
    ///    - languageCode: Language code in the lowercase for caching. For example: "en".
    /// - Returns: Cached object header URL.
    internal func createHeaderUrl(for key: String, languageCode: String?) throws -> URL {
        return try cacheUrl.appending(
            created: languageCode ?? defaultFolderName,
            withIntermediateDirectories: true
        ).appending(
            created: headerFolderKey
        ).appending(
            created: key,
            withIntermediateDirectories: true,
            isTheLastComponentAFile: true
        )
    }
}

