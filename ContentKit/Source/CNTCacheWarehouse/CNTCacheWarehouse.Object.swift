//
//  CNTCacheWarehouse.Object.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

extension CNTCacheWarehouse {
    /// URL to cache the file with the specified key and with the specified language code.
    /// - Parameters:
    ///    - key: Key for caching unique for the specified language code.
    ///    - languageCode: Language code in the lowercase for caching. For example: "en".
    /// - Returns: Cached object URL.
    public func objectUrl(for key: String, languageCode: String? = nil) -> URL {
        return cacheUrl.appending(languageCode ?? defaultFolderName).appending(objectFolderKey).appending(key)
    }
}
    
extension CNTCacheWarehouse {
    /// Creating a URL to cache the file with the specified key and with the specified language code.
    /// - Parameters:
    ///    - key: Key for caching unique for the specified language code.
    ///    - languageCode: Language code in the lowercase for caching. For example: "en".
    /// - Returns: Cached object URL.
    internal func createObjectUrl(for key: String, languageCode: String?) throws -> URL {
        return try cacheUrl.appending(
            created: languageCode ?? defaultFolderName,
            withIntermediateDirectories: true
        ).appending(
            created: objectFolderKey
        ).appending(
            created: key,
            withIntermediateDirectories: true,
            isTheLastComponentAFile: true
        )
    }
}

