//
//  CNTCacheWarehouse.Dequeue.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import UIKit

// MARK: Codable
extension CNTCacheWarehouse {
    /// Read object from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the requested object on the disk.
    ///    - version: Integer value of the version of the object. If the disc version of the object is lower than the requested one, the function will throw an error.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk. Default value is `nil`.
    ///    - decoder: Decoder used to decode an object from data. Default value is `JSONDecoder()`.
    /// - Returns: Codable object by specified key, version and language code
    public func dequeue<T: Codable>(for key: String, version: Int, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], decoder: AnyDecoder = JSONDecoder()) throws -> T {
        try header(for: key, languageCode: languageCode, target: version)
        
        return try CNTDataWarehouse.shared.load(from: objectUrl(for: key, languageCode: languageCode), readingOptions: readingOptions, decoder: decoder)
    }
}

// MARK: UIImage
extension CNTCacheWarehouse {
    /// Read object from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the requested object on the disk.
    ///    - version: Integer value of the version of the object. If the disc version of the object is lower than the requested one, the function will throw an error.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk. Default value is `nil`.
    /// - Returns: Data convertible object by specified key, version and language code.
    public func dequeue(for key: String, version: Int, languageCode: String? = nil, readingOptions: Data.ReadingOptions = []) throws -> UIImage {
        try header(for: key, languageCode: languageCode, target: version)
        
        return try CNTDataWarehouse.shared.load(from: objectUrl(for: key, languageCode: languageCode), readingOptions: readingOptions)
    }
    
    /// Read object from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the requested object on the disk.
    ///    - version: Integer value of the version of the object. If the disc version of the object is lower than the requested one, the function will throw an error.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk. Default value is `nil`.
    ///    - scale: Specified scale factor of image.
    /// - Returns: Data convertible object by specified key, version and language code.
    public func dequeue(for key: String, version: Int, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], scale: CGFloat) throws -> UIImage {
        try header(for: key, languageCode: languageCode, target: version)
        
        return try CNTDataWarehouse.shared.load(from: objectUrl(for: key, languageCode: languageCode), readingOptions: readingOptions, scale: scale)
    }
}

// MARK: NSMutableAttributedString
extension CNTCacheWarehouse {
    /// Read object from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the requested object on the disk.
    ///    - version: Integer value of the version of the object. If the disc version of the object is lower than the requested one, the function will throw an error.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk. Default value is `nil`.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    /// - Returns: Attributed string object by specified key, version and language code
    public func dequeue(
        mutableFor key: String,
        version: Int,
        languageCode: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
    ) throws -> NSMutableAttributedString {
        try header(for: key, languageCode: languageCode, target: version)
        
        return try CNTDataWarehouse.shared.load(mutableFrom: objectUrl(for: key, languageCode: languageCode), readingOptions: readingOptions, options: options, documentAttributes: dict)
    }
}

// MARK: NSAttributedString
extension CNTCacheWarehouse {
    /// Read object from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the requested object on the disk.
    ///    - version: Integer value of the version of the object. If the disc version of the object is lower than the requested one, the function will throw an error.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk. Default value is `nil`.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    /// - Returns: Attributed string object by specified key, version and language code
    public func dequeue(
        for key: String,
        version: Int,
        languageCode: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
    ) throws -> NSAttributedString {
        try header(for: key, languageCode: languageCode, target: version)
        
        return try CNTDataWarehouse.shared.load(from: objectUrl(for: key, languageCode: languageCode), readingOptions: readingOptions, options: options, documentAttributes: dict)
    }
}

// MARK: String
extension CNTCacheWarehouse {
    /// Read object from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the requested object on the disk.
    ///    - version: Integer value of the version of the object. If the disc version of the object is lower than the requested one, the function will throw an error.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk. Default value is `nil`.
    ///    - encoding: The encoding used to represent the data as string. Default value is `.utf8`.
    /// - Returns: String object by specified key, version and language code
    public func dequeue(for key: String, version: Int, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], encoding: String.Encoding = .utf8) throws -> String {
        try header(for: key, languageCode: languageCode, target: version)
        
        return try CNTDataWarehouse.shared.load(from: objectUrl(for: key, languageCode: languageCode), readingOptions: readingOptions, encoding: encoding)
    }
}

// MARK: Data
extension CNTCacheWarehouse {
    /// Read object from disk.
    /// - Parameters:
    ///    - key: The string value of the unique key for searching the requested object on the disk.
    ///    - version: Integer value of the version of the object. If the disc version of the object is lower than the requested one, the function will throw an error.
    ///    - languageCode: String-valued language code in lowercase. Part of the path to search for the requested object on disk. Default value is `nil`.
    /// - Returns: Data object by specified key, version and language code
    public func dequeue(for key: String, version: Int, languageCode: String? = nil, readingOptions: Data.ReadingOptions = []) throws -> Data {
        try header(for: key, languageCode: languageCode, target: version)
        
        return try CNTDataWarehouse.shared.load(from: objectUrl(for: key, languageCode: languageCode), readingOptions: readingOptions)
    }
}

