//
//  CNTRemoteTaskManager.UIImage.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import UIKit

// MARK: URLSessionDataTask+scale
extension CNTRemoteTaskManager {
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading image.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - scale: Specified scale factor of image.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func dataTask(
        from url: URL,
        id: String? = nil,
        scale: CGFloat,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<UIImage>
    ) throws -> URLSessionDataTask {
        return try dataTask(for: requestManager.request(from: url), id: id, scale: scale, progress: progress, completion: completion)
    }
    
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - scale: Specified scale factor of image.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func dataTask(
        for request: URLRequest,
        id: String? = nil,
        scale: CGFloat,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<UIImage>
    ) throws -> URLSessionDataTask {
        return try dataTask(
            for: request,
            id: id,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                
                guard let image = UIImage(data: data, scale: scale) else {
                    throw CNTError(description: "Invalid image data.", reason: "Request URL=“\(request.url?.description ?? "nil")“, id=“\(id ?? "nil")“, scale=“\(scale)“.")
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

// MARK: URLSessionDataTask
extension CNTRemoteTaskManager {
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading image.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func dataTask(
        from url: URL,
        id: String? = nil,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<UIImage>
    ) throws -> URLSessionDataTask {
        return try dataTask(for: requestManager.request(from: url), id: id, progress: progress, completion: completion)
    }
    
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func dataTask(
        for request: URLRequest,
        id: String? = nil,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<UIImage>
    ) throws -> URLSessionDataTask {
        return try dataTask(
            for: request,
            id: id,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                
                guard let image = UIImage(data: data) else {
                    throw CNTError(description: "Invalid image data.", reason: "Request URL=“\(request.url?.description ?? "nil")“, id=“\(id ?? "nil")“.")
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

// MARK: URLSessionDownloadTask+scale
extension CNTRemoteTaskManager {
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading image.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - scale: Specified scale factor of image.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func downloadTask(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        scale: CGFloat,
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<UIImage>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(for: requestManager.request(from: url), id: id, readingOptions: readingOptions, scale: scale, progress: progress, completion: completion)
    }
    
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - scale: Specified scale factor of image.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func downloadTask(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        scale: CGFloat,
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<UIImage>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(
            for: request,
            id: id,
            readingOptions: readingOptions,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                
                guard let image = UIImage(data: data, scale: scale) else {
                    throw CNTError(description: "Invalid image data.", reason: "Request URL=“\(request.url?.description ?? "nil")“, id=“\(id ?? "nil")“, scale=“\(scale)“.")
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

// MARK: URLSessionDownloadTask
extension CNTRemoteTaskManager {
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading image.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func downloadTask(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<UIImage>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(for: requestManager.request(from: url), id: id, readingOptions: readingOptions, progress: progress, completion: completion)
    }
    
    /// Attempt to download image from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested image or `nil` if fails.
    @discardableResult
    public func downloadTask(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<UIImage>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(
            for: request,
            id: id,
            readingOptions: readingOptions,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                
                guard let image = UIImage(data: data) else {
                    throw CNTError(description: "Invalid image data.", reason: "Request URL=“\(request.url?.description ?? "nil")“, id=“\(id ?? "nil")“.")
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

