//
//  CNTRemoteTaskManager.NSMutableAttributedString.swift
//  ContentKit
//
//  Created by Endoral on 27.02.2025.
//

import UIKit

// MARK: URLSessionDataTask
extension CNTRemoteTaskManager {
    /// Attempt to download attributed string from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading string.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested string or `nil` if fails.
    @discardableResult
    public func dataTask(
        mutableFrom url: URL,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<NSMutableAttributedString>
    ) throws -> URLSessionDataTask {
        return try dataTask(mutableFor: requestManager.request(from: url), id: id, options: options, documentAttributes: dict, progress: progress, completion: completion)
    }
    
    /// Attempt to download attributed string from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested string or `nil` if fails.
    @discardableResult
    public func dataTask(
        mutableFor request: URLRequest,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<NSMutableAttributedString>
    ) throws -> URLSessionDataTask {
        return try dataTask(
            for: request,
            id: id,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                let attributedString = try NSMutableAttributedString(data: data, options: options, documentAttributes: dict)
                
                completion(.success(attributedString))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

// MARK: URLSessionDownloadTask
extension CNTRemoteTaskManager {
    /// Attempt to download attributed string from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading string.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested string or `nil` if fails.
    @discardableResult
    public func downloadTask(
        mutableFrom url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<NSMutableAttributedString>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(mutableFor: requestManager.request(from: url), id: id, readingOptions: readingOptions, options: options, documentAttributes: dict, progress: progress, completion: completion)
    }
    
    /// Attempt to download attributed string from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested string or `nil` if fails.
    @discardableResult
    public func downloadTask(
        mutableFor request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<NSMutableAttributedString>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(
            for: request,
            id: id,
            readingOptions: readingOptions,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                let attributedString = try NSMutableAttributedString(data: data, options: options, documentAttributes: dict)
                
                completion(.success(attributedString))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

