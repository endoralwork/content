//
//  CNTRemoteTaskManager.Decodable.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

// MARK: URLSessionDataTask
extension CNTRemoteTaskManager {
    /// Attempt to download decodable object from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading decodable object.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - decoder: Decoder used to decode an object from data. Default value is `JSONDecoder()`.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested decodable object or `nil` if fails.
    @discardableResult
    public func dataTask<T: Decodable>(
        from url: URL,
        id: String? = nil,
        decoder: AnyDecoder = JSONDecoder(),
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<T>
    ) throws -> URLSessionDataTask {
        return try dataTask(for: requestManager.request(from: url), id: id, decoder: decoder, progress: progress, completion: completion)
    }
    
    /// Attempt to download decodable object from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - decoder: Decoder used to decode an object from data. Default value is `JSONDecoder()`.
    ///    - progress: The block called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested decodable object or `nil` if fails.
    @discardableResult
    public func dataTask<T: Decodable>(
        for request: URLRequest,
        id: String? = nil,
        decoder: AnyDecoder = JSONDecoder(),
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<T>
    ) throws -> URLSessionDataTask {
        return try dataTask(
            for: request,
            id: id,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                let object = try decoder.decode(T.self, from: data)
                
                completion(.success(object))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

// MARK: URLSessionDownloadTask
extension CNTRemoteTaskManager {
    /// Attempt to download decodable object from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading decodable object.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - decoder: Decoder used to decode an object from data. Default value is `JSONDecoder()`.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested decodable object or `nil` if fails.
    @discardableResult
    public func downloadTask<T: Decodable>(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        decoder: AnyDecoder = JSONDecoder(),
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<T>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(for: requestManager.request(from: url), id: id, readingOptions: readingOptions, decoder: decoder, progress: progress, completion: completion)
    }
    
    /// Attempt to download decodable object from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - decoder: Decoder used to decode an object from data. Default value is `JSONDecoder()`.
    ///    - progress: The block called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested decodable object or `nil` if fails.
    @discardableResult
    public func downloadTask<T: Decodable>(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        decoder: AnyDecoder = JSONDecoder(),
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<T>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(
            for: request,
            id: id,
            readingOptions: readingOptions,
            progress: progress
        ) { result in
            do {
                let data = try result.get() as Data
                let object = try decoder.decode(T.self, from: data)
                
                completion(.success(object))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

