//
//  CNTRemoteTaskManager.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

// MARK: Declaration
public final class CNTRemoteTaskManager {
    private let id = UUID().uuidString
    
    public let requestManager: CNTRemoteRequestManager
    
    private let taskSyncQueue = DispatchQueue(label: "com.ContentKit.CNTRemoteTaskManager.taskSync.DispatchQueue.\(UUID().uuidString)", attributes: .concurrent)
    private let handlerSyncQueue = DispatchQueue(label: "com.ContentKit.CNTRemoteTaskManager.handlerSync.DispatchQueue.\(UUID().uuidString)",attributes: .concurrent)
    
    private var dataTasks: Dictionary<URL, URLSessionDataTask> = [:]
    private var dataProgressHandlers: Dictionary<URL, Dictionary<URL, DataProgress>> = [:]
    private var dataCompletions: Dictionary<URL, Dictionary<URL, DataCompletion<Data>>> = [:]
    
    private var downloadTasks: Dictionary<URL, URLSessionDownloadTask> = [:]
    private var downloadProgressHandlers: Dictionary<URL, Dictionary<URL, DownloadProgress>> = [:]
    private var downloadCompletions: Dictionary<URL, Dictionary<URL, DownloadCompletion<Data>>> = [:]
    
    private init() {
        requestManager = CNTRemoteRequestManager.shared
    }
    
    public required init(with requestManager: CNTRemoteRequestManager) {
        assert(
            requestManager !== CNTRemoteRequestManager.shared,
            "\(CNTRemoteTaskManager.self): The use of the default-initialized shared instance \(CNTRemoteRequestManager.self) is prohibited. Create and use a new instance of \(CNTRemoteRequestManager.self) instead."
        )
        
        self.requestManager = requestManager
    }
}

// MARK: Shared
extension CNTRemoteTaskManager {
    /// The default initialized shared instance of this manager.
    public static let shared = CNTRemoteTaskManager()
}

// MARK: URLSessionDataTask
extension CNTRemoteTaskManager {
    internal func addDataTask(for url: URL, task: URLSessionDataTask) {
        taskSyncQueue.async(flags: .barrier) { [weak self] in
            self?.dataTasks[url] = task
        }
    }
    
    internal func removeDataTask(for url: URL) {
        taskSyncQueue.async(flags: .barrier) { [weak self] in
            self?.dataTasks.removeValue(forKey: url)
        }
    }
    
    internal func dataProgressHandlers(for url: URL) -> Dictionary<URL, DataProgress>? {
        return handlerSyncQueue.sync { [unowned self] in
            self.dataProgressHandlers[url]
        }
    }
    
    internal func addDataHandlers(for url: URL, with queryUrl: URL, progress: DataProgress?, completion: @escaping DataCompletion<Data>) {
        handlerSyncQueue.async(flags: .barrier) { [weak self] in
            if self?.dataProgressHandlers[url] == nil {
                if let progress = progress {
                    self?.dataProgressHandlers[url] = [queryUrl: progress]
                }
            } else {
                self?.dataProgressHandlers[url]?[queryUrl] = progress
            }
            
            if self?.dataCompletions[url] == nil {
                self?.dataCompletions[url] = [queryUrl: completion]
            } else {
                self?.dataCompletions[url]?[queryUrl] = completion
            }
        }
    }
    
    internal func removeDataHandlers(for url: URL, with result: Result<Data, Error>) {
        handlerSyncQueue.async(flags: .barrier) { [weak self] in
            self?.dataProgressHandlers.removeValue(forKey: url)
            
            self?.dataCompletions.removeValue(forKey: url)?.values.forEach { completion in
                completion(result)
            }
        }
    }
    
    public func allDataTasks() -> Dictionary<URL, URLSessionDataTask> {
        return taskSyncQueue.sync { [unowned self] in
            self.dataTasks
        }
    }
    
    public func dataTask(for url: URL) -> URLSessionDataTask? {
        return taskSyncQueue.sync { [unowned self] in
            self.dataTasks[url]
        }
    }
    
    public typealias DataProgress = CNTRemoteRequestManager.DataProgress
    public typealias DataCompletion<T> = (_ result: Result<T, Error>) -> Void
}

// MARK: URLSessionDownloadTask
extension CNTRemoteTaskManager {
    internal func addDownloadTask(for url: URL, task: URLSessionDownloadTask) {
        taskSyncQueue.async(flags: .barrier) { [weak self] in
            self?.downloadTasks[url] = task
        }
    }
    
    internal func removeDownloadTask(for url: URL) {
        taskSyncQueue.async(flags: .barrier) { [weak self] in
            self?.downloadTasks.removeValue(forKey: url)
        }
    }
    
    internal func downloadProgressHandlers(for url: URL) -> Dictionary<URL, DownloadProgress>? {
        return handlerSyncQueue.sync { [unowned self] in
            self.downloadProgressHandlers[url]
        }
    }
    
    internal func addDownloadHandlers(for url: URL, with queryUrl: URL, progress: DownloadProgress?, completion: @escaping DownloadCompletion<Data>) {
        handlerSyncQueue.async(flags: .barrier) { [weak self] in
            if self?.downloadProgressHandlers[url] == nil {
                if let progress = progress {
                    self?.downloadProgressHandlers[url] = [queryUrl: progress]
                }
            } else {
                self?.downloadProgressHandlers[url]?[queryUrl] = progress
            }
            
            if self?.downloadCompletions[url] == nil {
                self?.downloadCompletions[url] = [queryUrl: completion]
            } else {
                self?.downloadCompletions[url]?[queryUrl] = completion
            }
        }
    }
    
    internal func removeDownloadHandlers(for url: URL, with result: Result<Data, Error>) {
        handlerSyncQueue.async(flags: .barrier) { [weak self] in
            self?.downloadProgressHandlers.removeValue(forKey: url)
            
            self?.downloadCompletions.removeValue(forKey: url)?.values.forEach { completion in
                completion(result)
            }
        }
    }
    
    public func allDownloadTasks() -> Dictionary<URL, URLSessionDownloadTask> {
        return taskSyncQueue.sync { [unowned self] in
            self.downloadTasks
        }
    }
    
    public func downloadTask(for url: URL) -> URLSessionDownloadTask? {
        return taskSyncQueue.sync { [unowned self] in
            self.downloadTasks[url]
        }
    }
    
    public typealias DownloadProgress = CNTRemoteRequestManager.DownloadProgress
    public typealias DownloadCompletion<T> = (_ result: Result<T, Error>) -> Void
}

// MARK: Hashable
extension CNTRemoteTaskManager: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

// MARK: Equatable
extension CNTRemoteTaskManager: Equatable {
    public static func == (lhs: CNTRemoteTaskManager, rhs: CNTRemoteTaskManager) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

