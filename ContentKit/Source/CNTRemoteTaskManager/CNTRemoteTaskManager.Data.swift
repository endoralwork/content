//
//  CNTRemoteTaskManager.Data.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

// MARK: URLSessionDataTask
extension CNTRemoteTaskManager {
    /// Attempt to download data from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading data.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The handler called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested data or `nil` if fails.
    @discardableResult
    public func dataTask(
        from url: URL,
        id: String? = nil,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<Data>
    ) throws -> URLSessionDataTask {
        return try dataTask(for: requestManager.request(from: url), id: id, progress: progress, completion: completion)
    }
    
    /// Attempt to download data from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The handler called every time a piece of data is received. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested data or `nil` if fails.
    @discardableResult
    public func dataTask(
        for request: URLRequest,
        id: String? = nil,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion<Data>
    ) throws -> URLSessionDataTask {
        guard let url = request.url else {
            throw CNTError(description: "Invalid request URL.", reason: "Request URL=“\(request.url?.description ?? "nil")“, id=“\(id ?? "nil")“.")
        }
        
        let queryUrl: URL
        if let id {
            queryUrl = url.appending(id)
        } else {
            queryUrl = url
        }
        
        addDataHandlers(for: url, with: queryUrl, progress: progress, completion: completion)
        
        if let dataTask = dataTask(for: url) {
            return dataTask
        }
        
        let dataTask = requestManager.dataTask(
            for: request,
            progress: { [weak self] receivedData, totalReceivedData, totalBytesExpectedToWrite in
                self?.dataProgressHandlers(for: url)?.values.forEach { progressHandler in
                    progressHandler(receivedData, totalReceivedData, totalBytesExpectedToWrite)
                }
            }
        ) { [weak self] result in
            guard let self else { return }
            
            defer { self.removeDataTask(for: url) }
            
            do {
                let (data, _) = try result.get()
                
                self.removeDataHandlers(for: url, with: .success(data))
            } catch {
                self.removeDataHandlers(for: url, with: .failure(error))
            }
        }

        addDataTask(for: url, task: dataTask)

        return dataTask
    }
}

// MARK: URLSessionDownloadTask
extension CNTRemoteTaskManager {
    /// Attempt to download data from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - url: URL request address for loading data.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The handler called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested data or `nil` if fails.
    @discardableResult
    public func downloadTask(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<Data>
    ) throws -> URLSessionDownloadTask {
        return try downloadTask(for: requestManager.request(from: url), id: id, readingOptions: readingOptions, progress: progress, completion: completion)
    }
    
    /// Attempt to download data from the specified URL.
    /// A repeated request to the same address during the execution of the previous request remembers the completion, but does not duplicate the request itself.
    /// After the query is completed, all memorized completion for this query will be sequentially executed from the first to the last.
    /// - Parameters:
    ///    - request: A URL load request that is independent of protocol or URL scheme.
    ///    - id: String value that serves to prevent the completion of repeated calls for different requests to the same URL. If `nil`, the first closure specified for this url will be called.
    ///    - progress: The handler called every time a piece of data is writed. Default value is `nil`.
    ///    - completion: Executed in any case after completion of the data loading request. As parameter returns requested data or `nil` if fails.
    @discardableResult
    public func downloadTask(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion<Data>
    ) throws -> URLSessionDownloadTask {
        guard let url = request.url else {
            throw CNTError(description: "Invalid request URL.", reason: "Request URL=“\(request.url?.description ?? "nil")“, id=“\(id ?? "nil")“.")
        }
        
        let queryUrl: URL
        if let id {
            queryUrl = url.appending(id)
        } else {
            queryUrl = url
        }
        
        addDownloadHandlers(for: url, with: queryUrl, progress: progress, completion: completion)
        
        if let downloadTask = downloadTask(for: url) {
            return downloadTask
        }
        
        let downloadTask = requestManager.downloadTask(
            for: request,
            progress: { [weak self] bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                self?.downloadProgressHandlers(for: url)?.values.forEach { progressHandler in
                    progressHandler(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite)
                }
            }
        ) { [weak self] result in
            guard let self else { return }
            
            defer { self.removeDownloadTask(for: url) }
            
            do {
                let (location, _) = try result.get()
                let data = try CNTDataWarehouse.shared.load(from: location, readingOptions: readingOptions) as Data
                
                self.removeDownloadHandlers(for: url, with: .success(data))
            } catch {
                self.removeDownloadHandlers(for: url, with: .failure(error))
            }
        }

        addDownloadTask(for: url, task: downloadTask)

        return downloadTask
    }
}

