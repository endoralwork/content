//
//  CNTRemoteRequestManager.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import UIKit

// MARK: Declaration
/// Manager for asynchronous download of remote data using `URLSessionDownloadTask`.
public final class CNTRemoteRequestManager {
    private let id = UUID().uuidString
    private let sessionDelegate = CNTSessionDelegate()
    
    public let session: URLSession
    
    // #notice: shared instance constructor
    private convenience init() {
        let configuration: URLSessionConfiguration = .background(withIdentifier: "com.ContentKit.RemoteRequestManager.URLSessionConfiguration.background.shared")

        configuration.requestCachePolicy = CNTRemoteRequestManager.defaultRequestCachePolicy
        configuration.timeoutIntervalForRequest = CNTRemoteRequestManager.defaultRequestTimeout
        configuration.timeoutIntervalForResource = CNTRemoteRequestManager.defaultResourceTimeout
        
        let queue = OperationQueue()

        queue.qualityOfService = .background
        queue.maxConcurrentOperationCount = 25
        
        self.init(configuration: configuration, queue: queue)
    }
    
    /// Initialize with internal `URLSessionDownloadDelegate` object.
    /// - Parameters:
    ///    - configuration: A configuration object that defines behavior and policies for a `URLSession`.
    ///    - queue: A queue that regulates the execution of operations.
    public required init(configuration: URLSessionConfiguration, queue: OperationQueue? = nil) {
        session = URLSession(
            configuration: configuration,
            delegate: sessionDelegate,
            delegateQueue: queue
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onTerminate),
            name: UIApplication.willTerminateNotification,
            object: nil
        )
    }
    
    deinit {
        NotificationCenter.default.removeObserver(
            self,
            name: UIApplication.willTerminateNotification,
            object: nil
        )
    }
    
    @objc
    private func onTerminate() {
        session.invalidateAndCancel()
    }
}

// MARK: Shared
extension CNTRemoteRequestManager {
    /// The default initialized shared instance of this manager.
    public static let shared = CNTRemoteRequestManager()
    
    /// Request cache policy of used in that manager. Default value is `.reloadIgnoringLocalAndRemoteCacheData`.
    public static let defaultRequestCachePolicy: URLRequest.CachePolicy = .reloadIgnoringLocalAndRemoteCacheData
    
    /// Default timeout in seconds before canceling the request used in this manager. Default value is `60.0`.
    public static let defaultRequestTimeout: TimeInterval = 60.0
    
    /// Default maximum amount of time that a resource request should be allowed to take used in this manager. Default value is `60.0`.
    public static let defaultResourceTimeout: TimeInterval = 60.0
    
    public static let defaultHTTPMethod: HTTPMethod = .get
}

// MARK: Header response
extension CNTRemoteRequestManager {
    /// Downloading expected content size from the URL.
    /// - Parameters:
    ///    - url: The URL for the request.
    ///    - cachePolicy: The cache policy for the request. The default is `URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData`.
    ///    - timeout: The timeout interval for the request. The default is `60.0`.
    ///    - progress: The handler called every time a piece of data is received. The default is `nil`.
    ///    - completion: Request completion, returns expected content size in bytes for the request or error, if an occurred.
    /// - Returns: The new session data task.
    public func responseDataTask(
        for url: URL,
        cachePolicy: URLRequest.CachePolicy = CNTRemoteRequestManager.defaultRequestCachePolicy,
        timeout: TimeInterval = CNTRemoteRequestManager.defaultRequestTimeout,
        progress: DataProgress? = nil,
        completion: @escaping (_ result: Result<HTTPURLResponse, Error>) -> Void
    ) -> URLSessionDataTask {
        return dataTask(
            for: request(
                from: url,
                cachePolicy: cachePolicy,
                timeout: timeout,
                httpMethod: .head
            ),
            progress: progress,
            completion: { result in
                switch result {
                case .success((_, let response)):
                    completion(.success(response))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        )
    }
    
    /// Downloading expected content size from the URL.
    /// - Parameters:
    ///    - url: The URL for the request.
    ///    - cachePolicy: The cache policy for the request. The default is `URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData`.
    ///    - timeout: The timeout interval for the request. The default is `60.0`.
    ///    - progress: The handler called every time a piece of data is writed. The default is `nil`.
    ///    - completion: Request completion, returns expected content size in bytes for the request or error, if an occurred.
    /// - Returns: The new session download task.
    public func responseDownloadTask(
        for url: URL,
        cachePolicy: URLRequest.CachePolicy = CNTRemoteRequestManager.defaultRequestCachePolicy,
        timeout: TimeInterval = CNTRemoteRequestManager.defaultRequestTimeout,
        progress: DownloadProgress? = nil,
        completion: @escaping (_ result: Result<HTTPURLResponse, Error>) -> Void
    ) -> URLSessionDownloadTask {
        return downloadTask(
            for: request(
                from: url,
                cachePolicy: cachePolicy,
                timeout: timeout,
                httpMethod: .head
            ),
            progress: progress,
            completion: { result in
                switch result {
                case .success((_, let response)):
                    completion(.success(response))
                    
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        )
    }
}

// MARK: URLSessionDataTask
extension CNTRemoteRequestManager {
    /// Creating a task from url and parameters and run it.
    /// - Parameters:
    ///    - url: The URL for the request.
    ///    - progress: The handler called every time a piece of data is received. The default is `nil`.
    ///    - completion: Request completion, returns a response to the request, received data or error, if an occurred.
    /// - Returns: The new session data task.
    public func dataTask(
        from url: URL,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion
    ) -> URLSessionDataTask {
        return dataTask(
            for: request(from: url),
            progress: progress,
            completion: completion
        )
    }
    
    /// Creating a task from a request and run it.
    /// - Parameters:
    ///    - request: A URL request object that provides the URL, cache policy, request type, body data or body stream, and so on.
    ///    - progress: The handler called every time a piece of data is received. The default is `nil`.
    ///    - completion: Request completion, returns a response to the request, received data or error, if an occurred.
    /// - Returns: The new session data task.
    public func dataTask(
        for request: URLRequest,
        progress: DataProgress? = nil,
        completion: @escaping DataCompletion
    ) -> URLSessionDataTask {
        let dataTask = session.dataTask(with: request)
        
        sessionDelegate.prepare(dataTask: dataTask, progress: progress, completion: completion)
        
        return dataTask
    }
    
    /// - Parameters:
    ///    - bytesWritten: Data received in this call.
    ///    - totalBytesWritten: The total data of the downloading content transferred so far.
    ///    - totalBytesExpectedToWrite: The total expected length of the downloading content in bytes.
    public typealias DataProgress = (_ receivedData: Data, _ totalReceivedData: Data, _ totalBytesExpectedToWrite: Int64) -> Void
    
    /// - Parameters:
    ///    - data: Received content data.
    ///    - response: The server’s response to the completed task.
    ///    - error: An error object that indicates why the task failed.
    public typealias DataCompletion = (_ result: DataResult) -> Void
    
    /// - Parameters:
    ///    - data: Received content data.
    ///    - response: The server’s response to the completed task.
    ///    - error: An error object that indicates why the task failed.
    public typealias DataResult = Result<(data: Data, response: HTTPURLResponse), Error>
}
 
// MARK: URLSessionDownloadTask
extension CNTRemoteRequestManager {
    /// Creating a task from url and parameters and run it.
    /// - Parameters:
    ///    - url: The URL for the request.
    ///    - progress: The handler called every time a piece of data is writed. The default is `nil`.
    ///    - completion: Request completion, returns a response to the request, temporary content location or error, if an occurred.
    /// - Returns: The new session download task.
    public func downloadTask(
        from url: URL,
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion
    ) -> URLSessionDownloadTask {
        return downloadTask(
            for: request(from: url),
            progress: progress,
            completion: completion
        )
    }
    
    /// Creating a task from a request and run it.
    /// - Parameters:
    ///    - request: A URL request object that provides the URL, cache policy, request type, body data or body stream, and so on.
    ///    - progress: The handler called every time a piece of data is writed. The default is `nil`.
    ///    - completion: Request completion, returns a response to the request, temporary content location or error, if an occurred.
    /// - Returns: The new session download task.
    public func downloadTask(
        for request: URLRequest,
        progress: DownloadProgress? = nil,
        completion: @escaping DownloadCompletion
    ) -> URLSessionDownloadTask {
        let downloadTask = session.downloadTask(with: request)

        sessionDelegate.prepare(downloadTask: downloadTask, progress: progress, completion: completion)

        return downloadTask
    }
    
    /// - Parameters:
    ///    - bytesWritten: Number of bytes downloaded in this call.
    ///    - totalBytesWritten: The total length of the downloading content transferred so far in bytes.
    ///    - totalBytesExpectedToWrite: The total expected length of the downloading content in bytes.
    public typealias DownloadProgress = (_ bytesWritten: Int64, _ totalBytesWritten: Int64, _ totalBytesExpectedToWrite: Int64) -> Void
    
    /// - Parameters:
    ///    - data: Temporaty content location.
    ///    - response: The server’s response to the completed task.
    ///    - error: An error object that indicates why the task failed.
    public typealias DownloadCompletion = (_ result: DownloadResult) -> Void
    
    /// - Parameters:
    ///    - data: Temporaty content location.
    ///    - response: The server’s response to the completed task.
    ///    - error: An error object that indicates why the task failed.
    public typealias DownloadResult = Result<(location: URL, response: HTTPURLResponse), Error>
}

// MARK: URLRequest
extension CNTRemoteRequestManager {
    /// Creating a request from url and parameters and run it.
    /// - Parameters:
    ///    - url: The URL for the request.
    ///    - cachePolicy: The cache policy for the request. The default is `URLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData`.
    ///    - timeout: The timeout interval for the request. If `nil` then using current `URLSession.configuration.timeoutIntervalForRequest` value. The default is `60.0`.
    /// - Returns: The new request.
    public func request(
        from url: URL,
        cachePolicy: URLRequest.CachePolicy = CNTRemoteRequestManager.defaultRequestCachePolicy,
        timeout: TimeInterval? = nil,
        httpMethod: HTTPMethod = CNTRemoteRequestManager.defaultHTTPMethod
    ) -> URLRequest {
        var request = URLRequest(
            url: url,
            cachePolicy: cachePolicy,
            timeoutInterval: timeout ?? session.configuration.timeoutIntervalForRequest
        )
        
        request.httpMethod = httpMethod.rawValue
        
        return request
    }
    
    public enum HTTPMethod: String {
        case get = "GET"
        case post = "POST"
        case head = "HEAD"
    }
}

// MARK: Hashable
extension CNTRemoteRequestManager: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

// MARK: Equatable
extension CNTRemoteRequestManager: Equatable {
    public static func == (lhs: CNTRemoteRequestManager, rhs: CNTRemoteRequestManager) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

