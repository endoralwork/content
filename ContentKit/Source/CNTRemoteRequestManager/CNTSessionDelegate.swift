//
//  CNTSessionDelegate.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

// MARK: Declaration
internal final class CNTSessionDelegate: NSObject {
    private let handlerSyncQueue = DispatchQueue(label: "com.ContentKit.CNTSessionDelegate.handlerSync.DispatchQueue.\(UUID().uuidString)", attributes: .concurrent)

    private var totalReceivedData: Dictionary<Int, Data> = [:]
    private var dataProgressHandlers: Dictionary<Int, DataProgress> = [:]
    private var dataCompletions: Dictionary<Int, DataCompletion> = [:]
    
    private var downloadProgressHandlers: Dictionary<Int, DownloadProgress> = [:]
    private var downloadCompletions: Dictionary<Int, DownloadCompletion> = [:]
}

// MARK: URLSessionDataDelegate
extension CNTSessionDelegate: URLSessionDataDelegate {
    internal func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        updateProgress(for: dataTask, with: data)
    }
}
 
// MARK: URLSessionDownloadDelegate
extension CNTSessionDelegate: URLSessionDownloadDelegate {
    internal func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        updateProgress(for: downloadTask, written: bytesWritten, totalWritten: totalBytesWritten, totalExpected: totalBytesExpectedToWrite)
    }
    
    internal func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        finish(downloadTask: downloadTask, with: .success(location))
    }
}
    
// MARK: URLSessionTaskDelegate
extension CNTSessionDelegate: URLSessionTaskDelegate {
    internal func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        switch task {
        case let dataTask as URLSessionDataTask:
            if let error {
                finish(dataTask: dataTask, with: .failure(error))
            } else {
                finish(dataTask: dataTask, with: .success(()))
            }
            
        case let downloadTask as URLSessionDownloadTask:
            if let error {
                finish(downloadTask: downloadTask, with: .failure(error))
            } // #notice: else - do nothing, cause if error is nil then executed URLSessionDownloadDelegate.urlSession(_:downloadTask:didFinishDownloadingTo:)
            
        default:
            break
        }
    }
}

// MARK: URLSessionDataTask
extension CNTSessionDelegate {
    internal func prepare(dataTask: URLSessionDataTask, progress: DataProgress?, completion: @escaping DataCompletion) {
        handlerSyncQueue.sync { [weak self] in
            self?.totalReceivedData[dataTask.taskIdentifier] = Data()
            self?.dataProgressHandlers[dataTask.taskIdentifier] = progress
            self?.dataCompletions[dataTask.taskIdentifier] = completion
        }
    }
    
    private func updateProgress(for dataTask: URLSessionDataTask, with receivedData: Data) {
        handlerSyncQueue.sync { [weak self] in
            guard
                let totalBytesExpectedToWrite = dataTask.response?.expectedContentLength,
                let previousTotalReceivedData = self?.totalReceivedData[dataTask.taskIdentifier]
            else {
                return
            }
            
            let totalReceivedData = previousTotalReceivedData + receivedData
            
            self?.totalReceivedData[dataTask.taskIdentifier] = totalReceivedData
            self?.dataProgressHandlers[dataTask.taskIdentifier]?(receivedData, totalReceivedData, totalBytesExpectedToWrite)
        }
    }
    
    private func finish(dataTask: URLSessionDataTask, with result: Result<Void, Error>) {
        switch result {
        case .success:
            guard let httpResponse = dataTask.response as? HTTPURLResponse else {
                finish(dataTask: dataTask, with: .failure(CNTError(
                    description: "Invalid data url response.",
                    reason: "Original request URL=“\(dataTask.originalRequest?.url?.description ?? "nil")“, current request URL=“\(dataTask.currentRequest?.url?.description ?? "nil")“, response=“\(dataTask.response?.description ?? "nil")“."
                )))
                return
            }
            
            guard httpResponse.statusCode == 200 else {
                finish(dataTask: dataTask, with: .failure(CNTError(
                    description: "Invalid data url response status code \(httpResponse.statusCode).",
                    reason: "Original request URL=“\(dataTask.originalRequest?.url?.description ?? "nil")“, current request URL=“\(dataTask.currentRequest?.url?.description ?? "nil")“, response=“\(httpResponse.description)“."
                )))
                return
            }
            
            guard let data = handlerSyncQueue.sync(execute: { [weak self] in return self?.totalReceivedData.removeValue(forKey: dataTask.taskIdentifier) }) else {
                finish(dataTask: dataTask, with: .failure(CNTError(
                    description: "Total received data not found for data task.",
                    reason: "Original request URL=“\(dataTask.originalRequest?.url?.description ?? "nil")“, current request URL=“\(dataTask.currentRequest?.url?.description ?? "nil")“, response=“\(httpResponse.description)“."
                )))
                return
            }
            
            handlerSyncQueue.sync { [weak self] in
                self?.dataProgressHandlers.removeValue(forKey: dataTask.taskIdentifier)
                self?.dataCompletions.removeValue(forKey: dataTask.taskIdentifier)?(.success((data, httpResponse)))
            }
            
        case .failure(let error):
            handlerSyncQueue.sync { [weak self] in
                self?.totalReceivedData.removeValue(forKey: dataTask.taskIdentifier)
                self?.dataProgressHandlers.removeValue(forKey: dataTask.taskIdentifier)
                self?.dataCompletions.removeValue(forKey: dataTask.taskIdentifier)?(.failure(error))
            }
        }
    }
    
    internal typealias DataProgress = CNTRemoteRequestManager.DataProgress
    internal typealias DataCompletion = CNTRemoteRequestManager.DataCompletion
    internal typealias DataResult = CNTRemoteRequestManager.DataResult
}

// MARK: URLSessionDownloadTask
extension CNTSessionDelegate {
    internal func prepare(downloadTask: URLSessionDownloadTask, progress: DownloadProgress?, completion: @escaping DownloadCompletion) {
        handlerSyncQueue.sync { [weak self] in
            self?.downloadProgressHandlers[downloadTask.taskIdentifier] = progress
            self?.downloadCompletions[downloadTask.taskIdentifier] = completion
        }
    }
    
    private func updateProgress(for downloadTask: URLSessionDownloadTask, written bytesWritten: Int64, totalWritten totalBytesWritten: Int64, totalExpected totalBytesExpectedToWrite: Int64) {
        handlerSyncQueue.sync { [weak self] in
            self?.downloadProgressHandlers[downloadTask.taskIdentifier]?(bytesWritten, totalBytesWritten, totalBytesExpectedToWrite)
        }
    }
    
    private func finish(downloadTask: URLSessionDownloadTask, with result: Result<URL, Error>) {
        switch result {
        case .success(let location):
            guard let httpResponse = downloadTask.response as? HTTPURLResponse else {
                finish(downloadTask: downloadTask, with: .failure(CNTError(
                    description: "Invalid download url response.",
                    reason: "Original request URL=“\(downloadTask.originalRequest?.url?.description ?? "nil")“, current request URL=“\(downloadTask.currentRequest?.url?.description ?? "nil")“, response=“\(downloadTask.response?.description ?? "nil")“."
                )))
                return
            }
            
            guard httpResponse.statusCode == 200 else {
                finish(downloadTask: downloadTask, with: .failure(CNTError(
                    description: "Invalid download url response status code \(httpResponse.statusCode).",
                    reason: "Original request URL=“\(downloadTask.originalRequest?.url?.description ?? "nil")“, current request URL=“\(downloadTask.currentRequest?.url?.description ?? "nil")“, response=“\(httpResponse.description)“."
                )))
                return
            }
            
            handlerSyncQueue.sync { [weak self] in
                self?.downloadProgressHandlers.removeValue(forKey: downloadTask.taskIdentifier)
                self?.downloadCompletions.removeValue(forKey: downloadTask.taskIdentifier)?(.success((location, httpResponse))) // #todo: move file from location to permanent location and return it ?
            }
            
        case .failure(let error):
            handlerSyncQueue.sync { [weak self] in
                self?.downloadProgressHandlers.removeValue(forKey: downloadTask.taskIdentifier)
                self?.downloadCompletions.removeValue(forKey: downloadTask.taskIdentifier)?(.failure(error))
            }
        }
    }
    
    internal typealias DownloadProgress = CNTRemoteRequestManager.DownloadProgress
    internal typealias DownloadCompletion = CNTRemoteRequestManager.DownloadCompletion
    internal typealias DownloadResult = CNTRemoteRequestManager.DownloadResult
}

