//
//  AnyDecoder.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

public protocol AnyDecoder {
    var associatedEncoder: AnyEncoder { get }
    
    /// Decodes a top-level value of the given type from the given decoded representation.
    /// - Parameters:
    ///     - type: The type of the value to decode.
    ///     - data: The data to decode from.
    /// - Returns: A value of the requested type.
    /// - Throws:
    ///     - `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid.
    ///     - An error if any value throws an error during decoding.
    func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T
}

extension JSONDecoder: AnyDecoder {
    public var associatedEncoder: AnyEncoder {
        get { return JSONEncoder() }
    }
}

extension PropertyListDecoder: AnyDecoder {
    public var associatedEncoder: AnyEncoder {
        get { return PropertyListEncoder() }
    }
}

