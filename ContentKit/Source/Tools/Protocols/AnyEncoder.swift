//
//  AnyEncoder.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

public protocol AnyEncoder {
    var associatedDecoder: AnyDecoder { get }
    
    /// Encodes the given top-level value and returns its encoded representation.
    /// - Parameters:
    ///     - value: The value to encode.
    /// - Returns: A new `Data` value containing the encoded data.
    /// - Throws:
    ///     - `EncodingError.invalidValue` if a non-conforming floating-point value is encountered during encoding, and the encoding strategy is `.throw`.
    ///     - An error if any value throws an error during encoding.
    func encode<T: Encodable>(_ value: T) throws -> Data
}

extension JSONEncoder: AnyEncoder {
    public var associatedDecoder: AnyDecoder {
        get { return JSONDecoder() }
    }
}

extension PropertyListEncoder: AnyEncoder {
    public var associatedDecoder: AnyDecoder {
        get { return PropertyListDecoder() }
    }
}

