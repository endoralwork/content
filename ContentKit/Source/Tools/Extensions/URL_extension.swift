//
//  URL_extension.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

extension URL {
    func appending(
        created pathComponent: String,
        withIntermediateDirectories: Bool = false,
        isTheLastComponentAFile: Bool = false,
        attributes: Dictionary<FileAttributeKey, Any>? = nil
    ) throws -> URL {
        let url = appending(pathComponent)
        let fileManager = FileManager.default
        
        if fileManager.fileExists(atPath: url.path) {
            return url
        }
        
        let createdPath = isTheLastComponentAFile
            ? url.deletingLastPathComponent().path
            : url.path
        
        try fileManager.createDirectory(
            atPath: createdPath,
            withIntermediateDirectories: withIntermediateDirectories,
            attributes: attributes
        )
        
        return url
    }
    
    func appending(_ pathComponent: String) -> URL {
        return appendingPathComponent(pathComponent)
    }
}

extension URL {
    var key: String {
        get { return absoluteString.replacingOccurrences(of: "/", with: "").replacingOccurrences(of: ":", with: "") }
    }
}

