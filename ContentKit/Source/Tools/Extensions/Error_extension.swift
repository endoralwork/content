//
//  Error_extension.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation
 
extension Error {
    func info(_ message: String? = nil) -> String {
        let info = " - Description: \(localizedDescription)\n - Error: \(self)"
        
        guard let message else {
            return info
        }
        
        return "\(message)\n\(info)"
    }
}

