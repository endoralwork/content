//
//  CNTDataWarehouse.Load.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import UIKit

// MARK: Decodable
extension CNTDataWarehouse {
    /// Attempt to read object from disk.
    /// - Parameters:
    ///    - url: The url of the file whose contents you want.
    ///    - decoder: Decoder used to decode an object from data. Default value is `JSONDecoder()`.
    /// - Returns: Requested file as decodable object.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be read.
    ///    - `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid.
    ///    - An error if any value throws an error during decoding.
    public func load<T: Decodable>(from url: URL, readingOptions: Data.ReadingOptions = [], decoder: AnyDecoder = JSONDecoder()) throws -> T {
        switch T.self {
        case is Optional<NSAttributedString>.Type:
            let attributedString: NSAttributedString = try load(from: url, readingOptions: readingOptions)
            
            return attributedString as! T
            
        case is Optional<NSMutableAttributedString>:
            let mutableAttributedString: NSMutableAttributedString = try load(mutableFrom: url, readingOptions: readingOptions)
            
            return mutableAttributedString as! T

        case is Optional<String>.Type:
            let string: String = try load(from: url, readingOptions: readingOptions)
            
            return string as! T
            
        case is Optional<Data>.Type:
            let data: Data = try load(from: url, readingOptions: readingOptions)
            
            return data as! T

        default:
            let data: Data = try load(from: url, readingOptions: readingOptions)
            
            return try decoder.decode(T.self, from: data)
        }
    }
}

// MARK: UIImage
extension CNTDataWarehouse {
    /// Attempt to read image from disk.
    /// - Parameters:
    ///    - url: The url of the file whose contents you want.
    /// - Returns: Requested file as image.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be read.
    ///    - `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid.
    ///    - An error if any value throws an error during decoding.
    public func load(from url: URL, readingOptions: Data.ReadingOptions = []) throws -> UIImage {
        let data: Data = try load(from: url, readingOptions: readingOptions)
        
        guard let image = UIImage(data: data) else {
            throw CNTError(description: "Invalid image data.", reason: "URL=“\(url)“.")
        }
        
        return image
    }
    
    /// Attempt to read image from disk.
    /// - Parameters:
    ///    - url: The url of the file whose contents you want.
    ///    - scale: Specified scale factor of image.
    /// - Returns: Requested file as image.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be read.
    ///    - `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid.
    ///    - An error if any value throws an error during decoding.
    public func load(from url: URL, readingOptions: Data.ReadingOptions = [], scale: CGFloat) throws -> UIImage {
        let data: Data = try load(from: url, readingOptions: readingOptions)
        
        guard let image = UIImage(data: data, scale: scale) else {
            throw CNTError(description: "Invalid image data.", reason: "URL=“\(url)“, scale=“\(scale)“.")
        }
        
        return image
    }
}

// MARK: NSMutableAttributedString
extension CNTDataWarehouse {
    /// Attempt to read mutable attributed string from disk.
    /// - Parameters:
    ///    - url: The url of the file whose contents you want.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    /// - Returns: Requested file as attributed string.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be read.
    ///    - `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid.
    ///    - An error if any value throws an error during decoding.
    ///    - An in-out variable containing an encountered error, if any.
    public func load(
        mutableFrom url: URL,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
    ) throws -> NSMutableAttributedString {
        let data: Data = try load(from: url, readingOptions: readingOptions)
        
        return try NSMutableAttributedString(data: data, options: options, documentAttributes: dict)
    }
}

// MARK: NSAttributedString
extension CNTDataWarehouse {
    /// Attempt to read attributed string from disk.
    /// - Parameters:
    ///    - url: The url of the file whose contents you want.
    ///    - options: Document attributes for interpreting the document contents. If you pass an empty dictionary, the method examines the data to attempt to determine the appropriate attributes. Default value is `[]`.
    ///    - dict: An in-out dictionary containing document-level attributes described in NSAttributedString.DocumentAttributeKey. May be nil, in which case no document attributes are returned. Default value is `nil`.
    /// - Returns: Requested file as attributed string.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be read.
    ///    - `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid.
    ///    - An error if any value throws an error during decoding.
    ///    - An in-out variable containing an encountered error, if any.
    public func load(
        from url: URL,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
    ) throws -> NSAttributedString {
        let data: Data = try load(from: url, readingOptions: readingOptions)
        
        return try NSAttributedString(data: data, options: options, documentAttributes: dict)
    }
}

// MARK: String
extension CNTDataWarehouse {
    /// Attempt to read string from disk.
    /// - Parameters:
    ///    - url: The url of the file whose contents you want.
    ///    - encoding: The encoding used to represent the data as string. Default value is `.utf8`.
    /// - Returns: Requested file as string.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be read.
    ///    - `DecodingError.dataCorrupted` if values requested from the payload are corrupted, or if the given data is not valid.
    ///    - An error if any value throws an error during decoding.
    public func load(from url: URL, readingOptions: Data.ReadingOptions = [], encoding: String.Encoding = .utf8) throws -> String {
        let data: Data = try load(from: url, readingOptions: readingOptions)
        
        guard let string = String(data: data, encoding: encoding) else {
            throw CNTError(description: "Invalid string data.", reason: "URL=“\(url)“, encoding=“\(encoding)“.")
        }
        
        return string
    }
}
 
// MARK: Data
extension CNTDataWarehouse {
    /// Attempt to read data from disk.
    /// - Parameters:
    ///    - url: The url of the file whose contents you want.
    /// - Returns: Requested file as data.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be read.
    public func load(from url: URL, readingOptions: Data.ReadingOptions = []) throws -> Data {
        let path: String
        if #available(iOS 16.0, *) {
            path = url.path(percentEncoded: false)
        } else {
            path = url.path
        }
        
        guard let data = FileManager.default.contents(atPath: path) else {
            throw CNTError(description: "The file “\(url.lastPathComponent)” couldn’t be opened because there is no such file.", reason: "Path=“\(path)“.")
        }
        
        return data
    }
}

