//
//  CNTDataWarehouse.Remove.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

// MARK: remove
extension CNTDataWarehouse {
    /// Removes the file or directory at the specified URL.
    /// - Parameters:
    ///    - url: A file URL specifying the file or directory to remove. If the URL specifies a directory, the contents of that directory are recursively removed.
    /// - Throws:
    ///    - An error in the Cocoa domain, if `url` cannot be removed.
    public func remove(from url: URL) throws {
        try FileManager.default.removeItem(at: url)
    }
}

