//
//  CNTDataWarehouse.Save.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import UIKit

// MARK: Encodable
extension CNTDataWarehouse {
    /// Attempt to write object to disk, or remove from disk if object is `nil`.
    /// - Parameters:
    ///    - object: File as encodable object to write to disk.
    ///    - url: The location to write the encodable object into.
    ///    - encoder: Encoder used to encode data into an object. Default value is `JSONEncoder()`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    /// - Throws:
    ///    - `EncodingError.invalidValue` if a non-conforming floating-point value is encountered during encoding, and the encoding strategy is `.throw`.
    ///    - An error if any value throws an error during encoding.
    ///    - An error in the Cocoa domain, if there is an error writing to the `URL`.
    ///    - An error in the Cocoa domain, if `url` cannot be removed.
    public func save<T: Encodable>(_ object: T?, to url: URL, writingOptions: Data.WritingOptions = [], encoder: AnyEncoder = JSONEncoder()) throws {
        guard let object else {
            return
        }
        
        switch object {
        case let attrubutedString as NSAttributedString:
            try save(attrubutedString, to: url, writingOptions: writingOptions)
            
        case let string as String:
            try save(string, to: url, writingOptions: writingOptions)
            
        case let data as Data:
            try save(data, to: url, writingOptions: writingOptions)
            
        default:
            try save(object, to: url, writingOptions: writingOptions, encoder: encoder)
        }
    }
    
    /// Attempt to write object to disk.
    /// - Parameters:
    ///    - object: File as encodable object to write to disk.
    ///    - url: The location to write the encodable object into.
    ///    - encoder: Encoder used to encode data into an object. Default value is `JSONEncoder()`.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    /// - Throws:
    ///    - `EncodingError.invalidValue` if a non-conforming floating-point value is encountered during encoding, and the encoding strategy is `.throw`.
    ///    - An error if any value throws an error during encoding.
    ///    - An error in the Cocoa domain, if there is an error writing to the `URL`.
    public func save<T: Encodable>(_ object: T, to url: URL, writingOptions: Data.WritingOptions = [], encoder: AnyEncoder = JSONEncoder()) throws {
        let data = try encoder.encode(object)
        
        try save(data, to: url, writingOptions: writingOptions)
    }
}

// MARK: UIImage
extension CNTDataWarehouse {
    /// Attempt to write image to disk.
    /// - Parameters:
    ///    - image: Image to write to disk.
    ///    - url: The location to write the image into.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - dataType: Data format that contains in the specified image. Default value is `.png`.
    /// - Throws:
    ///    - `EncodingError.invalidValue` if a non-conforming floating-point value is encountered during encoding, and the encoding strategy is `.throw`.
    ///    - An error if any value throws an error during encoding.
    ///    - An error in the Cocoa domain, if there is an error writing to the `URL`.
    public func save(_ image: UIImage, to url: URL, writingOptions: Data.WritingOptions = [], dataType: ImageDataType = .png) throws {
        let data: Data
        
        switch dataType {
        case .png:
            guard let imageData = image.pngData() else {
                throw CNTError(description: "Unable to get png data from provided image.", reason: "URL=“\(url)“, dataType=“png“.")
            }
            
            data = imageData
            
        case .jpeg(let compressionQuality):
            guard let imageData = image.jpegData(compressionQuality: compressionQuality) else {
                throw CNTError(description: "Unable to get jpeg data from provided image.", reason: "URL=“\(url)“, dataType=“jpeg“, compressionQuality=“\(compressionQuality)“.")
            }
            
            data = imageData
        }
        
        try save(data, to: url, writingOptions: writingOptions)
    }
    
    public enum ImageDataType {
        case png
        case jpeg(compressionQuality: CGFloat)
    }
}

// MARK: NSAttributedString
extension CNTDataWarehouse {
    /// Attempt to write string to disk.
    /// - Parameters:
    ///    - string: String to write to disk.
    ///    - url: The location to write the string into.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - dict: A required dictionary specifying the document attributes. The dictionary contains values from Document Types and must at least contain documentType. Default value is `[:]`.
    /// - Throws:
    ///    - `EncodingError.invalidValue` if a non-conforming floating-point value is encountered during encoding, and the encoding strategy is `.throw`.
    ///    - An error if any value throws an error during encoding.
    ///    - An error in the Cocoa domain, if there is an error writing to the `URL`.
    ///    - An in-out variable containing an encountered error, if any.
    public func save(_ attributedString: NSAttributedString, to url: URL, writingOptions: Data.WritingOptions = [], documentAttributes dict: Dictionary<NSAttributedString.DocumentAttributeKey, Any> = [:]) throws {
        let data = try attributedString.data(from: NSRange(location: 0, length: attributedString.length), documentAttributes: dict)
        
        try save(data, to: url, writingOptions: writingOptions)
    }
}
 
// MARK: String
extension CNTDataWarehouse {
    /// Attempt to write string to disk.
    /// - Parameters:
    ///    - string: String to write to disk.
    ///    - url: The location to write the string into.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    ///    - encoding: The encoding used to represent the string as data. Default value is `.utf8`.
    /// - Throws:
    ///    - `EncodingError.invalidValue` if a non-conforming floating-point value is encountered during encoding, and the encoding strategy is `.throw`.
    ///    - An error if any value throws an error during encoding.
    ///    - An error in the Cocoa domain, if there is an error writing to the `URL`.
    public func save(_ string: String, to url: URL, writingOptions: Data.WritingOptions = [], encoding: String.Encoding = .utf8, allowLossyConversion: Bool = false) throws {
        guard let data = string.data(using: encoding, allowLossyConversion: allowLossyConversion) else {
            throw CNTError(description: "Unable to get data from provided string.", reason: "URL=“\(url)“, String=“\(string)“, encoding=“\(encoding)“, allowLossyConversion=“\(allowLossyConversion)“.")
        }
        
        try save(data, to: url, writingOptions: writingOptions)
    }
}
 
// MARK: Data
extension CNTDataWarehouse {
    /// Attempt to write data to disk.
    /// - Parameters:
    ///    - data: File in data representation to write to disk.
    ///    - url: The location to write the data into.
    ///    - writingOptions: Options for writing the data. Default value is `[]`.
    /// - Throws:
    ///    - An error in the Cocoa domain, if there is an error writing to the `URL`.
    public func save(_ data: Data, to url: URL, writingOptions: Data.WritingOptions = []) throws {
        try data.write(to: url, options: writingOptions)
    }
}

