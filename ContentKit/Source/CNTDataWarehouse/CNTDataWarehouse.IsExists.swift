//
//  CNTDataWarehouse.IsExists.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

// MARK: isExists
extension CNTDataWarehouse {
    /// Returns a Boolean value that indicates whether a file or directory exists at a specified URL.
    /// - Parameters:
    ///    - url: The URL of the file or directory.
    /// - Returns: `true` if a file at the specified URL exists, or `false` if the file does not exist or its existence could not be determined.
    public func isExists(at url: URL) -> Bool {
        return isExists(at: url.path)
    }
    
    /// Returns a Boolean value that indicates whether a file or directory exists at a specified path.
    /// - Parameters:
    ///    - path: The path of the file or directory. If path begins with a tilde (~), it must first be expanded with expandingTildeInPath; otherwise, this method returns false.
    ///     App Sandbox does not restrict which path values may be passed to this parameter.
    /// - Returns: `true` if a file at the specified path exists, or `false` if the file does not exist or its existence could not be determined.
    internal func isExists(at path: String) -> Bool {
        return FileManager.default.fileExists(atPath: path)
    }
}

