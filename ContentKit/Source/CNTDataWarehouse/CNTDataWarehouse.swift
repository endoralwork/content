//
//  CNTDataWarehouse.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

// MARK: Declaration
public final class CNTDataWarehouse {
    /// URL for application documents directory in user domain mask.
    public let documentsUrl: URL
    
    /// Address of the folder with the application files on the disk.
    public let applicationUrl: URL
    
    private init() {
        documentsUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        let appFolderName = Bundle.main.bundleIdentifier ?? "Application"
        
        do {
            applicationUrl = try documentsUrl.appending(created: appFolderName)
        } catch {
            fatalError(error.info("\(CNTDataWarehouse.self): Couldn't create application folder with name \(appFolderName)"))
        }
    }
}

// MARK: Shared
extension CNTDataWarehouse {
    public static let shared = CNTDataWarehouse()
    
    /// Address of the application bundle resources folder.
    public static let resourceURL = { () -> URL in
        guard let url = Bundle.main.resourceURL else {
            fatalError("\(CNTDataWarehouse.self): Couldn't get application resourceURL")
        }
        
        return url
    }()
}

