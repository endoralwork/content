//
//  CNTResourceManager.Codable.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

extension CNTResourceManager {
    public func codable<T: Codable>(from location: CodableLocation<T>) throws -> T {
        switch location {
        case .bundle(url: let url, readingOptions: let readingOptions, decoder: let decoder):
            return try bundledCodable(from: url, readingOptions: readingOptions, decoder: decoder)
            
        case .fileSystem(url: let url, readingOptions: let readingOptions, decoder: let decoder):
            return try fileSystemCodable(from: url, readingOptions: readingOptions, decoder: decoder)
            
        case .cache(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, decoder: let decoder):
            return try cachedCodable(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, decoder: decoder)
            
        case .local(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, decoder: let decoder):
            return try localCodable(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, decoder: decoder)
        }
    }
    
    public func codable<T: Codable>(from location: RemoteCodableLocation<T>) {
        switch location {
        case .remoteFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            decoder: let decoder,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadCodable(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    decoder: decoder,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            decoder: let decoder,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadCodable(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    decoder: decoder,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    public enum CodableLocation<T: Codable> {
        case bundle(url: URL, readingOptions: Data.ReadingOptions = [], decoder: AnyDecoder = JSONDecoder())
        case fileSystem(url: URL, readingOptions: Data.ReadingOptions = [], decoder: AnyDecoder = JSONDecoder())
        case cache(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], decoder: AnyDecoder = JSONDecoder())
        case local(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], decoder: AnyDecoder = JSONDecoder())
    }
    
    public enum RemoteCodableLocation<T: Codable> {
        case remoteFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            decoder: AnyDecoder = JSONDecoder(),
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<T>
        )
        
        case remoteFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            decoder: AnyDecoder = JSONDecoder(),
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<T>
        )
    }
}
 
extension CNTResourceManager {
    internal func localCodable<T: Codable>(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions, decoder: AnyDecoder) throws -> T {
        do {
            return try cachedCodable(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, decoder: decoder)
        } catch {
            do {
                return try fileSystemCodable(from: url, readingOptions: readingOptions, decoder: decoder)
            } catch {
                return try bundledCodable(from: url, readingOptions: readingOptions, decoder: decoder)
            }
        }
    }

    internal func cachedCodable<T: Codable>(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions, decoder: AnyDecoder) throws -> T {
        return try CNTCacheWarehouse.shared.dequeue(for: url.key, version: version, languageCode: languageCode, readingOptions: readingOptions, decoder: decoder)
    }

    internal func fileSystemCodable<T: Codable>(from url: URL, readingOptions: Data.ReadingOptions, decoder: AnyDecoder) throws -> T {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url

        return try CNTDataWarehouse.shared.load(from: fileUrl, readingOptions: readingOptions, decoder: decoder)
    }

    internal func bundledCodable<T: Codable>(from url: URL, readingOptions: Data.ReadingOptions, decoder: AnyDecoder) throws -> T {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)

        return try CNTDataWarehouse.shared.load(from: bundleUrl, readingOptions: readingOptions, decoder: decoder)
    }
    
    @discardableResult
    internal func codable<T: Codable>(
        for request: URLRequest,
        id: String?,
        decoder: AnyDecoder,
        cacheOptions: CacheOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<T>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(for: request, id: id, decoder: decoder, progress: progress) { result in
            do {
                let codable = try result.get() as T
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(codable, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions, encoder: decoder.associatedEncoder)
                case .doNotCache:
                    break
                }
                
                completion(.success(codable))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    @discardableResult
    internal func downloadCodable<T: Codable>(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        decoder: AnyDecoder,
        cacheOptions: CacheOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<T>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(for: request, id: id, readingOptions: readingOptions, decoder: decoder, progress: progress) { result in
            do {
                let codable = try result.get() as T
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(codable, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions, encoder: decoder.associatedEncoder)
                case .doNotCache:
                    break
                }
                
                completion(.success(codable))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

