//
//  CNTResourceManager.NSMutableAttributedString.swift
//  ContentKit
//
//  Created by Endoral on 27.02.2025.
//

import UIKit

extension CNTResourceManager {
    public func mutableAttributedString(from location: MutableAttributedStringLocation) throws -> NSMutableAttributedString {
        switch location {
        case .bundle(url: let url, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try bundledMutableAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            
        case .fileSystem(url: let url, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try fileSystemMutableAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            
        case .cache(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try cachedMutableAttributedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
            
        case .local(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try localMutableAttributedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
        }
    }
    
    public func mutableAttributedString(from location: RemoteMutableAttributedStringLocation) {
        switch location {
        case .remoteFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            options: let options,
            documentAttributes: let dict,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadMutableAttributedString(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    options: options,
                    documentAttributes: dict,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            options: let options,
            documentAttributes: let dict,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadMutableAttributedString(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    options: options,
                    documentAttributes: dict,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    public enum MutableAttributedStringLocation {
        case bundle(
            url: URL,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
        
        case fileSystem(
            url: URL,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
        
        case cache(
            url: URL,
            version: Int = 1,
            languageCode: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
        
        case local(
            url: URL,
            version: Int = 1,
            languageCode: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
    }
    
    public enum RemoteMutableAttributedStringLocation {
        case remoteFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<NSMutableAttributedString>
        )
        
        case remoteFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<NSMutableAttributedString>
        )
    }
}
 
extension CNTResourceManager {
    internal func localMutableAttributedString(
        from url: URL,
        version: Int,
        languageCode: String?,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSMutableAttributedString {
        do {
            return try cachedMutableAttributedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
        } catch {
            do {
                return try fileSystemMutableAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            } catch {
                return try bundledMutableAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            }
        }
    }
    
    internal func cachedMutableAttributedString(
        from url: URL,
        version: Int,
        languageCode: String?,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSMutableAttributedString {
        return try CNTCacheWarehouse.shared.dequeue(mutableFor: url.key, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
    }
    
    internal func fileSystemMutableAttributedString(
        from url: URL,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSMutableAttributedString {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url

        return try CNTDataWarehouse.shared.load(mutableFrom: fileUrl, readingOptions: readingOptions, options: options, documentAttributes: dict)
    }

    internal func bundledMutableAttributedString(
        from url: URL,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSMutableAttributedString {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)

        return try CNTDataWarehouse.shared.load(mutableFrom: bundleUrl, readingOptions: readingOptions, options: options, documentAttributes: dict)
    }
    
    @discardableResult
    internal func mutableAttributedString(
        for request: URLRequest,
        id: String?,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?,
        cacheOptions: CacheOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<NSMutableAttributedString>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(mutableFor: request, id: id, options: options, documentAttributes: dict, progress: progress) { result in
            do {
                let attributedString = try result.get() as NSMutableAttributedString

                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(
                        attributedString,
                        key: request.url!.key,
                        version: version,
                        languageCode: languageCode,
                        writingOptions: writingOptions,
                        documentAttributes: (dict?.pointee as? Dictionary<NSAttributedString.DocumentAttributeKey, Any>) ?? [:]
                    )
                case .doNotCache:
                    break
                }

                completion(.success(attributedString))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    @discardableResult
    internal func downloadMutableAttributedString(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?,
        cacheOptions: CacheOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<NSMutableAttributedString>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(mutableFor: request, id: id, readingOptions: readingOptions, options: options, documentAttributes: dict, progress: progress) { result in
            do {
                let attributedString = try result.get() as NSMutableAttributedString

                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(
                        attributedString,
                        key: request.url!.key,
                        version: version,
                        languageCode: languageCode,
                        writingOptions: writingOptions,
                        documentAttributes: (dict?.pointee as? Dictionary<NSAttributedString.DocumentAttributeKey, Any>) ?? [:]
                    )
                case .doNotCache:
                    break
                }

                completion(.success(attributedString))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

