//
//  CNTResourceManager.UIImage.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import UIKit

extension CNTResourceManager {
    public func image(from location: ImageLocation) throws -> UIImage {
        switch location {
        case .bundleScaled(url: let url, readingOptions: let readingOptions, scale: let scale):
            return try bundledImage(from: url, readingOptions: readingOptions, scale: scale)
        case .fileSystemScaled(url: let url, readingOptions: let readingOptions, scale: let scale):
            return try fileSystemImage(from: url, readingOptions: readingOptions, scale: scale)
        case .cacheScaled(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, scale: let scale):
            return try cachedImage(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, scale: scale)
        case .localScaled(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, scale: let scale):
            return try localImage(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, scale: scale)
            
        case .bundle(url: let url, readingOptions: let readingOptions):
            return try bundledImage(from: url, readingOptions: readingOptions)
        case .fileSystem(url: let url, readingOptions: let readingOptions):
            return try fileSystemImage(from: url, readingOptions: readingOptions)
        case .cache(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions):
            return try cachedImage(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions)
        case .local(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions):
            return try localImage(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions)
        }
    }
    
    public func image(from location: RemoteImageLocation) {
        switch location {
        case .remoteScaledFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            scale: let scale,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadImage(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    scale: scale,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteScaledFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            scale: let scale,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadImage(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    scale: scale,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadImage(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadImage(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    public enum ImageLocation {
        case bundleScaled(url: URL, readingOptions: Data.ReadingOptions = [], scale: CGFloat)
        case fileSystemScaled(url: URL, readingOptions: Data.ReadingOptions = [], scale: CGFloat)
        case cacheScaled(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], scale: CGFloat)
        case localScaled(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], scale: CGFloat)
        
        case bundle(url: URL, readingOptions: Data.ReadingOptions = [])
        case fileSystem(url: URL, readingOptions: Data.ReadingOptions = [])
        case cache(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [])
        case local(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [])
    }
    
    public enum RemoteImageLocation {
        case remoteScaledFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            scale: CGFloat,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<UIImage>
        )
        
        case remoteScaledFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            scale: CGFloat,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<UIImage>
        )
        
        case remoteFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<UIImage>
        )
        
        case remoteFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<UIImage>
        )
    }
}
 
// MARK: Scaled
extension CNTResourceManager {
    internal func localImage(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions, scale: CGFloat) throws -> UIImage {
        do {
            return try cachedImage(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, scale: scale)
        } catch {
            do {
                return try fileSystemImage(from: url, readingOptions: readingOptions, scale: scale)
            } catch {
                return try bundledImage(from: url, readingOptions: readingOptions, scale: scale)
            }
        }
    }
    
    internal func cachedImage(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions, scale: CGFloat) throws -> UIImage {
        return try CNTCacheWarehouse.shared.dequeue(for: url.key, version: version, languageCode: languageCode, readingOptions: readingOptions, scale: scale)
    }
    
    internal func fileSystemImage(from url: URL, readingOptions: Data.ReadingOptions, scale: CGFloat) throws -> UIImage {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url

        return try CNTDataWarehouse.shared.load(from: fileUrl, readingOptions: readingOptions, scale: scale)
    }

    internal func bundledImage(from url: URL, readingOptions: Data.ReadingOptions, scale: CGFloat) throws -> UIImage {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)

        return try CNTDataWarehouse.shared.load(from: bundleUrl, readingOptions: readingOptions, scale: scale)
    }
    
    @discardableResult
    internal func image(
        for request: URLRequest,
        id: String?,
        scale: CGFloat,
        cacheOptions: CacheOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<UIImage>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(for: request, id: id, scale: scale, progress: progress) { result in
            do {
                let image = try result.get() as UIImage
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(image, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                case .doNotCache:
                    break
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }

    @discardableResult
    internal func downloadImage(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        scale: CGFloat,
        cacheOptions: CacheOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<UIImage>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(for: request, id: id, readingOptions: readingOptions, scale: scale, progress: progress) { result in
            do {
                let image = try result.get() as UIImage
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(image, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                case .doNotCache:
                    break
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

// MARK: Default
extension CNTResourceManager {
    internal func localImage(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions) throws -> UIImage {
        do {
            return try cachedImage(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions)
        } catch {
            do {
                return try fileSystemImage(from: url, readingOptions: readingOptions)
            } catch {
                return try bundledImage(from: url, readingOptions: readingOptions)
            }
        }
    }
    
    internal func cachedImage(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions) throws -> UIImage {
        return try CNTCacheWarehouse.shared.dequeue(for: url.key, version: version, languageCode: languageCode, readingOptions: readingOptions)
    }
    
    internal func fileSystemImage(from url: URL, readingOptions: Data.ReadingOptions) throws -> UIImage {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url

        return try CNTDataWarehouse.shared.load(from: fileUrl, readingOptions: readingOptions)
    }

    internal func bundledImage(from url: URL, readingOptions: Data.ReadingOptions) throws -> UIImage {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)

        return try CNTDataWarehouse.shared.load(from: bundleUrl, readingOptions: readingOptions)
    }
    
    @discardableResult
    internal func image(
        for request: URLRequest,
        id: String?,
        cacheOptions: CacheOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<UIImage>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(for: request, id: id, progress: progress) { result in
            do {
                let image = try result.get() as UIImage
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(image, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                case .doNotCache:
                    break
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }

    @discardableResult
    internal func downloadImage(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        cacheOptions: CacheOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<UIImage>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(for: request, id: id, readingOptions: readingOptions, progress: progress) { result in
            do {
                let image = try result.get() as UIImage
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(image, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                case .doNotCache:
                    break
                }
                
                completion(.success(image))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

