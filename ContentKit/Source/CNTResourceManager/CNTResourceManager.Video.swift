//
//  CNTResourceManager.Video.swift
//  ContentKit
//
//  Created by Ad min on 03.03.2025.
//

import Foundation

extension CNTResourceManager {
    public func video(from location: VideoLocation) throws -> URL {
        switch location {
        case .bundle(url: let url):
            return try bundledVideoUrl(from: url)
            
        case .fileSystem(url: let url):
            return try fileSystemVideoUrl(from: url)
            
        case .cache(url: let url, version: let version, languageCode: let languageCode):
            return try cachedVideoUrl(from: url, version: version, languageCode: languageCode)
            
        case .local(url: let url, version: let version, languageCode: let languageCode):
            return try localVideoUrl(from: url, version: version, languageCode: languageCode)
        }
    }
    
    public func video(from location: RemoteVideoLocation) {
        switch location {
        case .remoteFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            version: let version,
            languageCode: let languageCode,
            writingOptions: let writingOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadVideo(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    version: version,
                    languageCode: languageCode,
                    writingOptions: writingOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            version: let version,
            languageCode: let languageCode,
            writingOptions: let writingOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadVideo(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    version: version,
                    languageCode: languageCode,
                    writingOptions: writingOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    public enum VideoLocation {
        case bundle(url: URL)
        case fileSystem(url: URL)
        case cache(url: URL, version: Int = 1, languageCode: String? = nil)
        case local(url: URL, version: Int = 1, languageCode: String? = nil)
    }
    
    public enum RemoteVideoLocation {
        case remoteFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            version: Int = 1,
            languageCode: String? = nil,
            writingOptions: Data.WritingOptions = [],
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<URL>
        )
        
        case remoteFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            version: Int = 1,
            languageCode: String? = nil,
            writingOptions: Data.WritingOptions = [],
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<URL>
        )
    }
}
 
extension CNTResourceManager {
    internal func localVideoUrl(from url: URL, version: Int, languageCode: String?) throws -> URL {
        do {
            return try cachedVideoUrl(from: url, version: version, languageCode: languageCode)
        } catch {
            do {
                return try fileSystemVideoUrl(from: url)
            } catch {
                return try bundledVideoUrl(from: url)
            }
        }
    }
    
    internal func cachedVideoUrl(from url: URL, version: Int, languageCode: String?) throws -> URL {
        try CNTCacheWarehouse.shared.header(for: url.key, languageCode: languageCode, target: version)
        
        let cacheUrl = CNTCacheWarehouse.shared.objectUrl(for: url.key, languageCode: languageCode)
        
        guard CNTDataWarehouse.shared.isExists(at: cacheUrl) else {
            throw CNTError(
                description: "The file “\(url.lastPathComponent)” couldn’t be opened because there is no such file.",
                reason: "Key URL=“\(url)“, cache URL=“\(cacheUrl)“, key=“\(url.key)“, version=“\(version)“, languageCode=“\(languageCode ?? "nil")“."
            )
        }
        
        return cacheUrl
    }
    
    internal func fileSystemVideoUrl(from url: URL) throws -> URL {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url
        
        guard CNTDataWarehouse.shared.isExists(at: fileUrl) else {
            throw CNTError(description: "The file “\(url.lastPathComponent)” couldn’t be opened because there is no such file.", reason: "Key URL=“\(url)“, file URL=“\(fileUrl)“.")
        }
        
        return fileUrl
    }
    
    internal func bundledVideoUrl(from url: URL) throws -> URL {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)
        
        guard CNTDataWarehouse.shared.isExists(at: bundleUrl) else {
            throw CNTError(description: "The file “\(url.lastPathComponent)” couldn’t be opened because there is no such file.", reason: "Key URL=“\(url)“, bundle URL=“\(bundleUrl)“.")
        }
        
        return bundleUrl
    }
    
    @discardableResult
    internal func video(
        for request: URLRequest,
        id: String?,
        version: Int,
        languageCode: String?,
        writingOptions: Data.WritingOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<URL>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(for: request, id: id, progress: progress) { result in
            do {
                let data = try result.get() as Data
                
                let (_, videoUrl) = try CNTCacheWarehouse.shared.enqueue(data, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                
                completion(.success(videoUrl))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    @discardableResult
    internal func downloadVideo(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        version: Int,
        languageCode: String?,
        writingOptions: Data.WritingOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<URL>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(for: request, id: id, readingOptions: readingOptions, progress: progress) { result in
            do {
                let data = try result.get() as Data
                
                let (_, videoUrl) = try CNTCacheWarehouse.shared.enqueue(data, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                
                completion(.success(videoUrl))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

