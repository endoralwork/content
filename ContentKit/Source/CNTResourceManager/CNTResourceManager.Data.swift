//
//  CNTResourceManager.Data.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

extension CNTResourceManager {
    public func data(from location: DataLocation) throws -> Data {
        switch location {
        case .bundle(url: let url, readingOptions: let readingOptions):
            return try bundledData(from: url, readingOptions: readingOptions)
            
        case .fileSystem(url: let url, readingOptions: let readingOptions):
            return try fileSystemData(from: url, readingOptions: readingOptions)
            
        case .cache(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions):
            return try cachedData(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions)
            
        case .local(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions):
            return try localData(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions)
        }
    }
    
    public func data(from location: RemoteDataLocation) {
        switch location {
        case .remoteFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadData(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadData(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    public enum DataLocation {
        case bundle(url: URL, readingOptions: Data.ReadingOptions = [])
        case fileSystem(url: URL, readingOptions: Data.ReadingOptions = [])
        case cache(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [])
        case local(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [])
    }
    
    public enum RemoteDataLocation {
        case remoteFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<Data>
        )
        
        case remoteFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<Data>
        )
    }
}
 
extension CNTResourceManager {
    internal func localData(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions) throws -> Data {
        do {
            return try cachedData(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions)
        } catch {
            do {
                return try fileSystemData(from: url, readingOptions: readingOptions)
            } catch {
                return try bundledData(from: url, readingOptions: readingOptions)
            }
        }
    }
    
    internal func cachedData(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions) throws -> Data {
        return try CNTCacheWarehouse.shared.dequeue(for: url.key, version: version, languageCode: languageCode, readingOptions: readingOptions)
    }
    
    internal func fileSystemData(from url: URL, readingOptions: Data.ReadingOptions) throws -> Data {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url
        
        return try CNTDataWarehouse.shared.load(from: fileUrl, readingOptions: readingOptions)
    }
    
    internal func bundledData(from url: URL, readingOptions: Data.ReadingOptions) throws -> Data {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)
        
        return try CNTDataWarehouse.shared.load(from: bundleUrl, readingOptions: readingOptions)
    }
    
    @discardableResult
    internal func data(
        for request: URLRequest,
        id: String?,
        cacheOptions: CacheOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<Data>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(for: request, id: id, progress: progress) { result in
            do {
                let data = try result.get() as Data
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(data, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                case .doNotCache:
                    break
                }
                
                completion(.success(data))
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    @discardableResult
    internal func downloadData(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        cacheOptions: CacheOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<Data>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(for: request, id: id, readingOptions: readingOptions, progress: progress) { result in
            do {
                let data = try result.get() as Data
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(data, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions)
                case .doNotCache:
                    break
                }
                
                completion(.success(data))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

