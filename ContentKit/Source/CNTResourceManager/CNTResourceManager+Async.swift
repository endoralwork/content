//
//  CNTResourceManager+Async.swift
//  ResourceManager
//
//  Created by Endoral on 26.02.2025.
//

import UIKit

// MARK: URLSessionDataTask+Codable
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func codable<Value: Codable>(
        from url: URL,
        id: String? = nil,
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Value {
        return try await codable(
            from: url,
            id: id,
            decoder: decoder,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func codable<Value: Codable>(
        for request: URLRequest,
        id: String? = nil,
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Value {
        return try await codable(
            for: request,
            id: id,
            decoder: decoder,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func codable<Value: Codable>(
        from url: URL,
        id: String? = nil,
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<Value> {
        return try codable(
            for: taskManager.requestManager.request(from: url),
            id: id,
            decoder: decoder,
            cacheOptions: cacheOptions
        )
    }
    
    public func codable<Value: Codable>(
        for request: URLRequest,
        id: String? = nil,
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<Value> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<Value>()

        try codable(
            for: request,
            id: id,
            decoder: decoder,
            cacheOptions: cacheOptions,
            progress: { receivedData, totalReceivedData, totalBytesExpectedToWrite in
                streamContinuation.yield((receivedData, totalReceivedData, totalBytesExpectedToWrite))
            },
            completion: { result in
                do {
                    let value = try result.get() as Value
                    
                    streamContinuation.finish()
                    asyncResult.resume(returning: value)
                } catch {
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+Codable
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadCodable<Value: Codable>(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Value {
        return try await downloadCodable(
            from: url,
            id: id,
            readingOptions: readingOptions,
            decoder: decoder,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadCodable<Value: Codable>(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Value {
        return try await downloadCodable(
            for: request,
            id: id,
            readingOptions: readingOptions,
            decoder: decoder,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadCodable<Value: Codable>(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<Value> {
        return try downloadCodable(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            decoder: decoder,
            cacheOptions: cacheOptions
        )
    }
    
    public func downloadCodable<Value: Codable>(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        decoder: AnyDecoder = JSONDecoder(),
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<Value> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<Value>()

        try downloadCodable(
            for: request,
            id: id,
            readingOptions: readingOptions,
            decoder: decoder,
            cacheOptions: cacheOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                do {
                    let value = try result.get() as Value
                    
                    streamContinuation.finish()
                    asyncResult.resume(returning: value)
                } catch {
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDataTask+UIImage+scaled
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func image(
        from url: URL,
        id: String? = nil,
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await image(
            from: url,
            id: id,
            scale: scale,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func image(
        for request: URLRequest,
        id: String? = nil,
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await image(
            for: request,
            id: id,
            scale: scale,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func image(
        from url: URL,
        id: String? = nil,
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<UIImage> {
        return try image(
            for: taskManager.requestManager.request(from: url),
            id: id,
            scale: scale,
            cacheOptions: cacheOptions
        )
    }
    
    public func image(
        for request: URLRequest,
        id: String? = nil,
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<UIImage> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<UIImage>()
        
        try image(
            for: request,
            id: id,
            scale: scale,
            cacheOptions: cacheOptions,
            progress: { receivedData, totalReceivedData, totalBytesExpectedToWrite in
                streamContinuation.yield((receivedData, totalReceivedData, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let image):
                    streamContinuation.finish()
                    asyncResult.resume(returning: image)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDataTask+UIImage
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func image(
        from url: URL,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await image(
            from: url,
            id: id,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func image(
        for request: URLRequest,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await image(
            for: request,
            id: id,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func image(
        from url: URL,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<UIImage> {
        return try image(
            for: taskManager.requestManager.request(from: url),
            id: id,
            cacheOptions: cacheOptions
        )
    }
    
    public func image(
        for request: URLRequest,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<UIImage> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<UIImage>()
        
        try image(
            for: request,
            id: id,
            cacheOptions: cacheOptions,
            progress: { receivedData, totalReceivedData, totalBytesExpectedToWrite in
                streamContinuation.yield((receivedData, totalReceivedData, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let image):
                    streamContinuation.finish()
                    asyncResult.resume(returning: image)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+UIImage+scaled
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadImage(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await downloadImage(
            from: url,
            id: id,
            readingOptions: readingOptions,
            scale: scale,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadImage(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await downloadImage(
            for: request,
            id: id,
            readingOptions: readingOptions,
            scale: scale,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadImage(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<UIImage> {
        return try downloadImage(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            scale: scale,
            cacheOptions: cacheOptions
        )
    }
    
    public func downloadImage(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        scale: CGFloat,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<UIImage> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<UIImage>()
        
        try downloadImage(
            for: request,
            id: id,
            readingOptions: readingOptions,
            scale: scale,
            cacheOptions: cacheOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let image):
                    streamContinuation.finish()
                    asyncResult.resume(returning: image)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+UIImage
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadImage(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await downloadImage(
            from: url,
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadImage(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) async throws -> UIImage {
        return try await downloadImage(
            for: request,
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadImage(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<UIImage> {
        return try downloadImage(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions
        )
    }
    
    public func downloadImage(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<UIImage> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<UIImage>()
        
        try downloadImage(
            for: request,
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let image):
                    streamContinuation.finish()
                    asyncResult.resume(returning: image)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDataTask+NSMutableAttributedString
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func mutableAttributedString(
        from url: URL,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSMutableAttributedString {
        return try await mutableAttributedString(
            from: url,
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func mutableAttributedString(
        for request: URLRequest,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSMutableAttributedString {
        return try await mutableAttributedString(
            for: request,
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func mutableAttributedString(
        from url: URL,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<NSMutableAttributedString> {
        return try mutableAttributedString(
            for: taskManager.requestManager.request(from: url),
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        )
    }
    
    public func mutableAttributedString(
        for request: URLRequest,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<NSMutableAttributedString> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<NSMutableAttributedString>()
        
        try mutableAttributedString(
            for: request,
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions,
            progress: { receivedData, totalReceivedData, totalBytesExpectedToWrite in
                streamContinuation.yield((receivedData, totalReceivedData, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let attributedString):
                    streamContinuation.finish()
                    asyncResult.resume(returning: attributedString)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+NSMutableAttributedString
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadMutableAttributedString(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSMutableAttributedString {
        return try await downloadMutableAttributedString(
            from: url,
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadMutableAttributedString(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSMutableAttributedString {
        return try await downloadMutableAttributedString(
            for: request,
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadMutableAttributedString(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<NSMutableAttributedString> {
        return try downloadMutableAttributedString(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        )
    }
    
    public func downloadMutableAttributedString(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<NSMutableAttributedString> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<NSMutableAttributedString>()
        
        try downloadMutableAttributedString(
            for: request,
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let attributedString):
                    streamContinuation.finish()
                    asyncResult.resume(returning: attributedString)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDataTask+NSAttributedString
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func attributedString(
        from url: URL,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSAttributedString {
        return try await attributedString(
            from: url,
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func attributedString(
        for request: URLRequest,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSAttributedString {
        return try await attributedString(
            for: request,
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func attributedString(
        from url: URL,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<NSAttributedString> {
        return try attributedString(
            for: taskManager.requestManager.request(from: url),
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        )
    }
    
    public func attributedString(
        for request: URLRequest,
        id: String? = nil,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<NSAttributedString> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<NSAttributedString>()
        
        try attributedString(
            for: request,
            id: id,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions,
            progress: { receivedData, totalReceivedData, totalBytesExpectedToWrite in
                streamContinuation.yield((receivedData, totalReceivedData, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let attributedString):
                    streamContinuation.finish()
                    asyncResult.resume(returning: attributedString)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+NSAttributedString
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadAttributedString(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSAttributedString {
        return try await downloadAttributedString(
            from: url,
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadAttributedString(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> NSAttributedString {
        return try await downloadAttributedString(
            for: request,
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadAttributedString(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<NSAttributedString> {
        return try downloadAttributedString(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions
        )
    }
    
    public func downloadAttributedString(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
        documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<NSAttributedString> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<NSAttributedString>()
        
        try downloadAttributedString(
            for: request,
            id: id,
            readingOptions: readingOptions,
            options: options,
            documentAttributes: documentAttributes,
            cacheOptions: cacheOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let attributedString):
                    streamContinuation.finish()
                    asyncResult.resume(returning: attributedString)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDataTask+String
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func string(
        from url: URL,
        id: String? = nil,
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> String {
        return try await string(
            from: url,
            id: id,
            encoding: encoding,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func string(
        for request: URLRequest,
        id: String? = nil,
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> String {
        return try await string(
            for: request,
            id: id,
            encoding: encoding,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func string(
        from url: URL,
        id: String? = nil,
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<String> {
        return try string(
            for: taskManager.requestManager.request(from: url),
            id: id,
            encoding: encoding,
            cacheOptions: cacheOptions
        )
    }
    
    public func string(
        for request: URLRequest,
        id: String? = nil,
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<String> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<String>()
        
        try string(
            for: request,
            id: id,
            encoding: encoding,
            cacheOptions: cacheOptions,
            progress: { receivedData, totalReceivedData, totalBytesExpectedToWrite in
                streamContinuation.yield((receivedData, totalReceivedData, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let string):
                    streamContinuation.finish()
                    asyncResult.resume(returning: string)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+String
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadString(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> String {
        return try await downloadString(
            from: url,
            id: id,
            readingOptions: readingOptions,
            encoding: encoding,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadString(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> String {
        return try await downloadString(
            for: request,
            id: id,
            readingOptions: readingOptions,
            encoding: encoding,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadString(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<String> {
        return try downloadString(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            encoding: encoding,
            cacheOptions: cacheOptions
        )
    }
    
    public func downloadString(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        encoding: String.Encoding = .utf8,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<String> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<String>()
        
        try downloadString(
            for: request,
            id: id,
            readingOptions: readingOptions,
            encoding: encoding,
            cacheOptions: cacheOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let string):
                    streamContinuation.finish()
                    asyncResult.resume(returning: string)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDataTask+Data
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func data(
        from url: URL,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Data {
        return try await data(
            from: url,
            id: id,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func data(
        for request: URLRequest,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Data {
        return try await data(
            for: request,
            id: id,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func data(
        from url: URL,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<Data> {
        return try data(
            for: taskManager.requestManager.request(from: url),
            id: id,
            cacheOptions: cacheOptions
        )
    }
    
    public func data(
        for request: URLRequest,
        id: String? = nil,
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDataProgress<Data> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<Data>()
        
        try data(
            for: request,
            id: id,
            cacheOptions: cacheOptions,
            progress: { receivedData, totalReceivedData, totalBytesExpectedToWrite in
                streamContinuation.yield((receivedData, totalReceivedData, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let data):
                    streamContinuation.finish()
                    asyncResult.resume(returning: data)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDataTask+Video
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func video(
        from url: URL,
        id: String? = nil,
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) async throws -> URL {
        return try await video(
            from: url,
            id: id,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions
        ).result.value
    }
    
    public func video(
        for request: URLRequest,
        id: String? = nil,
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) async throws -> URL {
        return try await video(
            for: request,
            id: id,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions
        ).result.value
    }
    
    public func video(
        from url: URL,
        id: String? = nil,
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) throws -> AsyncDataProgress<URL> {
        return try video(
            for: taskManager.requestManager.request(from: url),
            id: id,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions
        )
    }
    
    public func video(
        for request: URLRequest,
        id: String? = nil,
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) throws -> AsyncDataProgress<URL> {
        let (stream, streamContinuation) = AsyncDataProgressStream.makeStream()
        let asyncResult = AsyncResult<URL>()
        
        try video(
            for: request,
            id: id,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let data):
                    streamContinuation.finish()
                    asyncResult.resume(returning: data)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+Video
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadVideo(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) async throws -> URL {
        return try await downloadVideo(
            from: url,
            id: id,
            readingOptions: readingOptions,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions
        ).result.value
    }
    
    public func downloadVideo(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) async throws -> URL {
        return try await downloadVideo(
            for: request,
            id: id,
            readingOptions: readingOptions,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions
        ).result.value
    }
    
    public func downloadVideo(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) throws -> AsyncDownloadProgress<URL> {
        return try downloadVideo(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions
        )
    }
    
    public func downloadVideo(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        version: Int = 1,
        languageCode: String? = nil,
        writingOptions: Data.WritingOptions = []
    ) throws -> AsyncDownloadProgress<URL> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<URL>()
        
        try downloadVideo(
            for: request,
            id: id,
            readingOptions: readingOptions,
            version: version,
            languageCode: languageCode,
            writingOptions: writingOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let data):
                    streamContinuation.finish()
                    asyncResult.resume(returning: data)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}

// MARK: URLSessionDownloadTask+Data
@available(iOS 13.0, *)
extension CNTResourceManager {
    public func downloadData(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Data {
        return try await downloadData(
            from: url,
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadData(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) async throws -> Data {
        return try await downloadData(
            for: request,
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions
        ).result.value
    }
    
    public func downloadData(
        from url: URL,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<Data> {
        return try downloadData(
            for: taskManager.requestManager.request(from: url),
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions
        )
    }
    
    public func downloadData(
        for request: URLRequest,
        id: String? = nil,
        readingOptions: Data.ReadingOptions = [],
        cacheOptions: CacheOptions = .cache()
    ) throws -> AsyncDownloadProgress<Data> {
        let (stream, streamContinuation) = AsyncDownloadProgressStream.makeStream()
        let asyncResult = AsyncResult<Data>()
        
        try downloadData(
            for: request,
            id: id,
            readingOptions: readingOptions,
            cacheOptions: cacheOptions,
            progress: { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                streamContinuation.yield((bytesWritten, totalBytesWritten, totalBytesExpectedToWrite))
            },
            completion: { result in
                switch result {
                case .success(let data):
                    streamContinuation.finish()
                    asyncResult.resume(returning: data)
                    
                case .failure(let error):
                    streamContinuation.finish(throwing: error)
                    asyncResult.resume(throwing: error)
                }
            }
        ).resume()
        
        return (stream, asyncResult)
    }
}
 
// MARK: Types
@available(iOS 13.0, *)
extension CNTResourceManager {
    public typealias AsyncDataProgress<Value> = (stream: AsyncDataProgressStream, result: AsyncResult<Value>)
    public typealias AsyncDataProgressStream = AsyncThrowingStream<AsyncDataProgressValue, Error>
    public typealias AsyncDataProgressValue = (receivedData: Data, totalReceivedData: Data, totalBytesExpectedToWrite: Int64)
    
    public typealias AsyncDownloadProgress<Value> = (stream: AsyncDownloadProgressStream, result: AsyncResult<Value>)
    public typealias AsyncDownloadProgressStream = AsyncThrowingStream<AsyncDownloadProgressValue, Error>
    public typealias AsyncDownloadProgressValue = (bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64)
    
    public struct AsyncResult<Value> {
        private let stream: ResultStream
        private let streamContinuation: ResultStream.Continuation
        
        public var value: Value {
            get async throws {
                guard let value = try await stream.first(where: { _ in true }) else {
                    throw CNTError(description: "Invalid value.", reason: "Unexpected found “nil“ while stream waiting for value.")
                }
                
                return value
            }
        }
        
        internal init() {
            (stream, streamContinuation) = ResultStream.makeStream()
        }
        
        internal func resume(with result: sending Result<Value, Error>) {
            switch result {
            case .success(let value):
                resume(returning: value)
            case .failure(let error):
                resume(throwing: error)
            }
        }
        
        internal func resume(returning value: sending Value) {
            streamContinuation.yield(value)
            streamContinuation.finish()
        }
        
        internal func resume(throwing error: Error) {
            streamContinuation.finish(throwing: error)
        }
        
        private typealias ResultStream = AsyncThrowingStream<Value, Error>
    }
}

