//
//  CNTResourceManager.String.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

extension CNTResourceManager {
    public func string(from location: StringLocation) throws -> String {
        switch location {
        case .bundle(url: let url, readingOptions: let readingOptions, encoding: let encoding):
            return try bundledString(from: url, readingOptions: readingOptions, encoding: encoding)
            
        case .fileSystem(url: let url, readingOptions: let readingOptions, encoding: let encoding):
            return try fileSystemString(from: url, readingOptions: readingOptions, encoding: encoding)
            
        case .cache(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, encoding: let encoding):
            return try cachedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, encoding: encoding)
            
        case .local(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, encoding: let encoding):
            return try localString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, encoding: encoding)
        }
    }
    
    public func string(from location: RemoteStringLocation) {
        switch location {
        case .remoteFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            encoding: let encoding,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadString(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    encoding: encoding,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            encoding: let encoding,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadString(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    encoding: encoding,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    public enum StringLocation {
        case bundle(url: URL, readingOptions: Data.ReadingOptions = [], encoding: String.Encoding = .utf8)
        case fileSystem(url: URL, readingOptions: Data.ReadingOptions = [], encoding: String.Encoding = .utf8)
        case cache(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], encoding: String.Encoding = .utf8)
        case local(url: URL, version: Int = 1, languageCode: String? = nil, readingOptions: Data.ReadingOptions = [], encoding: String.Encoding = .utf8)
    }
    
    public enum RemoteStringLocation {
        case remoteFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            encoding: String.Encoding = .utf8,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<String>
        )
        
        case remoteFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            encoding: String.Encoding = .utf8,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<String>
        )
    }
}
 
extension CNTResourceManager {
    internal func localString(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions, encoding: String.Encoding) throws -> String {
        do {
            return try cachedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, encoding: encoding)
        } catch {
            do {
                return try fileSystemString(from: url, readingOptions: readingOptions, encoding: encoding)
            } catch {
                return try bundledString(from: url, readingOptions: readingOptions, encoding: encoding)
            }
        }
    }
    
    internal func cachedString(from url: URL, version: Int, languageCode: String?, readingOptions: Data.ReadingOptions, encoding: String.Encoding) throws -> String {
        return try CNTCacheWarehouse.shared.dequeue(for: url.key, version: version, languageCode: languageCode, readingOptions: readingOptions, encoding: encoding)
    }
    
    internal func fileSystemString(from url: URL, readingOptions: Data.ReadingOptions, encoding: String.Encoding) throws -> String {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url

        return try CNTDataWarehouse.shared.load(from: fileUrl, readingOptions: readingOptions, encoding: encoding)
    }

    internal func bundledString(from url: URL, readingOptions: Data.ReadingOptions, encoding: String.Encoding) throws -> String {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)

        return try CNTDataWarehouse.shared.load(from: bundleUrl, readingOptions: readingOptions, encoding: encoding)
    }
    
    @discardableResult
    internal func string(
        for request: URLRequest,
        id: String?,
        encoding: String.Encoding,
        cacheOptions: CacheOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<String>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(for: request, id: id, encoding: encoding, progress: progress) { result in
            do {
                let string = try result.get() as String
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(string, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions, encoding: encoding)
                case .doNotCache:
                    break
                }
                
                completion(.success(string))
            } catch {
                completion(.failure(error))
            }
        }
    }

    @discardableResult
    internal func downloadString(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        encoding: String.Encoding,
        cacheOptions: CacheOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<String>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(for: request, id: id, readingOptions: readingOptions, encoding: encoding, progress: progress) { result in
            do {
                let string = try result.get() as String
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(string, key: request.url!.key, version: version, languageCode: languageCode, writingOptions: writingOptions, encoding: encoding)
                case .doNotCache:
                    break
                }
                
                completion(.success(string))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

