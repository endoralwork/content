//
//  CNTResourceManager.NSAttributedString.swift
//  ContentKit
//
//  Created by Endoral on 22.10.2024.
//  Copyright © 2024 Endoral. All rights reserved.
//

import UIKit

extension CNTResourceManager {
    public func attributedString(from location: AttributedStringLocation) throws -> NSAttributedString {
        switch location {
        case .bundle(url: let url, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try bundledAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            
        case .fileSystem(url: let url, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try fileSystemAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            
        case .cache(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try cachedAttributedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
            
        case .local(url: let url, version: let version, languageCode: let languageCode, readingOptions: let readingOptions, options: let options, documentAttributes: let dict):
            return try localAttributedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
        }
    }
    
    public func attributedString(from location: RemoteAttributedStringLocation) {
        switch location {
        case .remoteFrom(
            url: let url,
            id: let id,
            readingOptions: let readingOptions,
            options: let options,
            documentAttributes: let dict,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadAttributedString(
                    for: taskManager.requestManager.request(from: url),
                    id: id,
                    readingOptions: readingOptions,
                    options: options,
                    documentAttributes: dict,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
            
        case .remoteFor(
            request: let request,
            id: let id,
            readingOptions: let readingOptions,
            options: let options,
            documentAttributes: let dict,
            cacheOptions: let cacheOptions,
            progress: let progress,
            completion: let completion
        ):
            do {
                try downloadAttributedString(
                    for: request,
                    id: id,
                    readingOptions: readingOptions,
                    options: options,
                    documentAttributes: dict,
                    cacheOptions: cacheOptions,
                    progress: progress,
                    completion: completion
                ).resume()
            } catch {
                completion(.failure(error))
            }
        }
    }
    
    public enum AttributedStringLocation {
        case bundle(
            url: URL,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
        
        case fileSystem(
            url: URL,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
        
        case cache(
            url: URL,
            version: Int = 1,
            languageCode: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
        
        case local(
            url: URL,
            version: Int = 1,
            languageCode: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil
        )
    }
    
    public enum RemoteAttributedStringLocation {
        case remoteFrom(
            url: URL,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<NSAttributedString>
        )
        
        case remoteFor(
            request: URLRequest,
            id: String? = nil,
            readingOptions: Data.ReadingOptions = [],
            options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any> = [:],
            documentAttributes: AutoreleasingUnsafeMutablePointer<NSDictionary?>? = nil,
            cacheOptions: CacheOptions = .cache(),
            progress: DownloadProgress? = nil,
            completion: DownloadCompletion<NSAttributedString>
        )
    }
}
 
extension CNTResourceManager {
    internal func localAttributedString(
        from url: URL,
        version: Int,
        languageCode: String?,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSAttributedString {
        do {
            return try cachedAttributedString(from: url, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
        } catch {
            do {
                return try fileSystemAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            } catch {
                return try bundledAttributedString(from: url, readingOptions: readingOptions, options: options, documentAttributes: dict)
            }
        }
    }
    
    internal func cachedAttributedString(
        from url: URL,
        version: Int,
        languageCode: String?,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSAttributedString {
        return try CNTCacheWarehouse.shared.dequeue(for: url.key, version: version, languageCode: languageCode, readingOptions: readingOptions, options: options, documentAttributes: dict)
    }
    
    internal func fileSystemAttributedString(
        from url: URL,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSAttributedString {
        let fileUrl = url.scheme == nil ? URL(fileURLWithPath: url.path) : url

        return try CNTDataWarehouse.shared.load(from: fileUrl, readingOptions: readingOptions, options: options, documentAttributes: dict)
    }

    internal func bundledAttributedString(
        from url: URL,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?
    ) throws -> NSAttributedString {
        let bundleUrl = CNTDataWarehouse.resourceURL.appending(url.path)

        return try CNTDataWarehouse.shared.load(from: bundleUrl, readingOptions: readingOptions, options: options, documentAttributes: dict)
    }
    
    @discardableResult
    internal func attributedString(
        for request: URLRequest,
        id: String?,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?,
        cacheOptions: CacheOptions,
        progress: DataProgress?,
        completion: @escaping DataCompletion<NSAttributedString>
    ) throws -> URLSessionDataTask {
        return try taskManager.dataTask(for: request, id: id, options: options, documentAttributes: dict, progress: progress) { result in
            do {
                let attributedString = try result.get() as NSAttributedString
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(
                        attributedString,
                        key: request.url!.key,
                        version: version,
                        languageCode: languageCode,
                        writingOptions: writingOptions,
                        documentAttributes: (dict?.pointee as? Dictionary<NSAttributedString.DocumentAttributeKey, Any>) ?? [:]
                    )
                case .doNotCache:
                    break
                }
                
                completion(.success(attributedString))
            } catch {
                completion(.failure(error))
            }
        }
    }

    @discardableResult
    internal func downloadAttributedString(
        for request: URLRequest,
        id: String?,
        readingOptions: Data.ReadingOptions,
        options: Dictionary<NSAttributedString.DocumentReadingOptionKey, Any>,
        documentAttributes dict: AutoreleasingUnsafeMutablePointer<NSDictionary?>?,
        cacheOptions: CacheOptions,
        progress: DownloadProgress?,
        completion: @escaping DownloadCompletion<NSAttributedString>
    ) throws -> URLSessionDownloadTask {
        return try taskManager.downloadTask(for: request, id: id, readingOptions: readingOptions, options: options, documentAttributes: dict, progress: progress) { result in
            do {
                let attributedString = try result.get() as NSAttributedString
                
                switch cacheOptions {
                case .cache(let version, let languageCode, let writingOptions):
                    try CNTCacheWarehouse.shared.enqueue(
                        attributedString,
                        key: request.url!.key,
                        version: version,
                        languageCode: languageCode,
                        writingOptions: writingOptions,
                        documentAttributes: (dict?.pointee as? Dictionary<NSAttributedString.DocumentAttributeKey, Any>) ?? [:]
                    )
                case .doNotCache:
                    break
                }
                
                completion(.success(attributedString))
            } catch {
                completion(.failure(error))
            }
        }
    }
}

