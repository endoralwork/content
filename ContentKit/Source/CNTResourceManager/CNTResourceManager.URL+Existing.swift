//
//  CNTResourceManager.URL+Existing.swift
//  ContentKit
//
//  Created by Endoral on 12/04/2022.
//  Copyright © 2022 Endoral. All rights reserved.
//

import Foundation

// MARK: URL of an existing file
extension CNTResourceManager {
    public func existingContentUrl(for location: ExistingContentUrlLocation) -> URL? {
        guard iContentExist(at: location) else {
            return nil
        }
        
        switch location {
        case .bundle(url: let url):
            return contentUrl(for: .bundle(url: url))
            
        case .fileSystem(url: let url):
            return contentUrl(for: .fileSystem(url: url))
            
        case .cache(url: let url, version: _, languageCode: let languageCode):
            return contentUrl(for: .cache(url: url, languageCode: languageCode))
            
        case .local(url: let url, version: let version, languageCode: let languageCode):
            return existingContentUrl(for: .cache(url: url, version: version, languageCode: languageCode))
            ?? existingContentUrl(for: .fileSystem(url: url))
            ?? existingContentUrl(for: .bundle(url: url))
        }
    }
    
    public typealias ExistingContentUrlLocation = ContentExistingLocation
}

// MARK: Content existing
extension CNTResourceManager {
    public func iContentExist(at location: ContentExistingLocation) -> Bool {
        switch location {
        case .bundle(url: let url):
            return isBundledDataExist(at: url)
            
        case .fileSystem(url: let url):
            return isFileSystemDataExist(at: url)
            
        case .cache(url: let url, version: let version, languageCode: let languageCode):
            return isCachedDataExist(at: url, version: version, languageCode: languageCode)
            
        case .local(url: let url, version: let version, languageCode: let languageCode):
            return isLocalDataExist(at: url, version: version, languageCode: languageCode)
        }
    }
    
    private func isLocalDataExist(at url: URL, version: Int, languageCode: String? = nil) -> Bool {
        if isCachedDataExist(at: url, version: version, languageCode: languageCode) {
            return true
        }
        
        if isFileSystemDataExist(at: url) {
            return true
        }
        
        if isBundledDataExist(at: url) {
            return true
        }
        
        return false
    }
    
    private func isCachedDataExist(at url: URL, version: Int, languageCode: String? = nil) -> Bool {
        let key = url.key
        
        do {
            try CNTCacheWarehouse.shared.header(for: key, languageCode: languageCode, target: version)
        } catch {
            return false
        }
        
        let url = CNTCacheWarehouse.shared.objectUrl(for: key, languageCode: languageCode)
        
        guard CNTDataWarehouse.shared.isExists(at: url) else {
            return false
        }
        
        return true
    }

    private func isFileSystemDataExist(at url: URL) -> Bool {
        return CNTDataWarehouse.shared.isExists(at: fileSystemDataUrl(for: url))
    }

    private func isBundledDataExist(at url: URL) -> Bool {
        return CNTDataWarehouse.shared.isExists(at: bundledDataUrl(for: url))
    }
    
    public enum ContentExistingLocation {
        case bundle(url: URL)
        case fileSystem(url: URL)
        case cache(url: URL, version: Int = 1, languageCode: String? = nil)
        case local(url: URL, version: Int = 1, languageCode: String? = nil)
    }
}

// MARK: Content URL
extension CNTResourceManager {
    public func contentUrl(for location: ContentUrlLocation) -> URL {
        switch location {
        case .bundle(url: let url):
            return bundledDataUrl(for: url)
            
        case .fileSystem(url: let url):
            return fileSystemDataUrl(for: url)
            
        case .cache(url: let url, languageCode: let languageCode):
            return cachedDataUrl(for: url, languageCode: languageCode)
        }
    }
    
    private func cachedDataUrl(for url: URL, languageCode: String? = nil) -> URL {
        return CNTCacheWarehouse.shared.objectUrl(for: url.key, languageCode: languageCode)
    }
    
    private func fileSystemDataUrl(for url: URL) -> URL {
        return url.scheme == nil ? URL(fileURLWithPath: url.path) : url
    }

    private func bundledDataUrl(for url: URL) -> URL {
        return CNTDataWarehouse.resourceURL.appending(url.path)
    }
    
    public enum ContentUrlLocation {
        case bundle(url: URL)
        case fileSystem(url: URL)
        case cache(url: URL, languageCode: String? = nil)
    }
}

