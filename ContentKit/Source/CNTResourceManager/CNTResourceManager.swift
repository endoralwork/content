//
//  CNTResourceManager.swift
//  ContentKit
//
//  Created by Endoral on 01/10/2021.
//  Copyright © 2021 Endoral. All rights reserved.
//

import Foundation

// MARK: Declaration
public final class CNTResourceManager {
    private let id = UUID().uuidString
    
    public let taskManager: CNTRemoteTaskManager
    public let name: String
    
    private init() {
        taskManager = CNTRemoteTaskManager.shared
        name = "shared"
    }
    
    public required init(with taskManager: CNTRemoteTaskManager, name: String = "") {
        assert(
            taskManager !== CNTRemoteTaskManager.shared,
            "\(CNTResourceManager.self): The use of the default-initialized shared instance \(CNTRemoteTaskManager.self) is prohibited. Create and use a new instance \(CNTRemoteTaskManager.self)."
        )
        
        self.taskManager = taskManager
        self.name = name
    }
}

// MARK: Shared
extension CNTResourceManager {
    /// The default initialized shared instance of this manager.
    public static let shared = CNTResourceManager()
}

// MARK: Types
extension CNTResourceManager {
    public enum CacheOptions {
        case cache(version: Int = 1, languageCode: String? = nil, writingOptions: Data.WritingOptions = [])
        case doNotCache
    }
    
    public typealias DataProgress = CNTRemoteTaskManager.DataProgress
    public typealias DataCompletion<T> = CNTRemoteTaskManager.DataCompletion<T>
    
    public typealias DownloadProgress = CNTRemoteTaskManager.DownloadProgress
    public typealias DownloadCompletion<T> = CNTRemoteTaskManager.DownloadCompletion<T>
}

// MARK: Hashable
extension CNTResourceManager: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

// MARK: Equatable
extension CNTResourceManager: Equatable {
    public static func == (lhs: CNTResourceManager, rhs: CNTResourceManager) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
}

