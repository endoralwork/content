//
//  CNTError.swift
//  ContentKit
//
//  Created by Endoral on 26.02.2025.
//

import Foundation

// MARK: Declaration
public class CNTError: NSError, @unchecked Sendable {
    internal init(
        description: String,
        reason: String? = nil,
        code: Int = 0,
        file: String = #file,
        function: String = #function,
        line: Int = #line
    ) {
        let domain = "com.ContentKit.error"
        
        super.init(
            domain: domain,
            code: code,
            userInfo: [
                NSLocalizedDescriptionKey: description,
                NSLocalizedFailureReasonErrorKey: reason as Any,
                "\(domain).file": file,
                "\(domain).function": function,
                "\(domain).line": line
            ]
        )
    }

    internal required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

