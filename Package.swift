// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "ContentKit",
    products: [
        .library(name: "ContentKit", type: .static, targets: ["ContentKit"])
    ],
    targets: [
        .target(
            name: "ContentKit",
            path: "ContentKit"
        )
    ]
)

